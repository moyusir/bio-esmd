import sympy
import sys

def generate_shake_series(natoms, constraints, name=""):
  r = [[sympy.symbols('r_{%d%d}' % (i, j)) for j in range(natoms)] for i in range(natoms)]
  d = [[sympy.symbols('d_{%d%d}' % (i, j)) for j in range(natoms)] for i in range(natoms)]
  vp = [[sympy.symbols('v^{p}_{%d%d}' % (i, j)) for j in range(natoms)] for i in range(natoms)]
  m = [sympy.symbols("m%s" % (i)) for i in range(natoms)]
  l = [sympy.symbols("l%s" % (i)) for i in range(len(constraints))]
  A = sympy.Matrix([[0 for i in range(len(constraints))] for j in range(len(constraints))])
  dt = sympy.symbols('\\varDelta{t}')
  for ik, [k1, k2] in enumerate(constraints):
    for ikk, [kk1, kk2] in enumerate(constraints):
      elem = 0
      delta11 = int(k1 == kk1)
      delta12 = int(k1 == kk2)
      delta21 = int(k2 == kk1)
      delta22 = int(k2 == kk2)
      elem = ((delta11 - delta12) / m[k1] + (delta22 - delta21) / m[k2]) * r[kk1][kk2] * r[k1][k2]
      A[ik, ikk] = elem
  c = sympy.Matrix([[0] for i in range(len(constraints))])
  for ik, [k1, k2] in enumerate(constraints):
    c[ik, 0] = -r[k1][k2] * vp[k1][k2]
  print('\\begin{equation}')
  print('A_{%s}=' % name)
  print(sympy.latex(A))
  print('\\end{equation}')
  print('\\begin{equation}')
  print('c_{%s}=' % name)
  print(sympy.latex(c))
  print('\\end{equation}')

print('\\section{RATTLE of CH}')
generate_shake_series(2, [[0, 1]], "CH")
print('\\section{RATTLE of CH2}')
generate_shake_series(3, [[0, 1], [0, 2]], "CH2")
print('\\section{RATTLE of OH2}')
generate_shake_series(3, [[0, 1], [0, 2], [1, 2]], "OH2")
print('\\section{RATTLE of CH3}')
generate_shake_series(4, [[0, 1], [0, 2], [0, 3]], "CH3")
