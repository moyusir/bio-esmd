import re
COMMENT_RE = re.compile("[*!].*")
ret = []
stat = None
def add_param(f, bonds, angles, dihedrals, impropers, nonbs):
    for line in open(f):
        line = COMMENT_RE.sub("", line).strip()
        try:
            if line:
                if len(line.split()) == 1:
                    stat = line[:4].lower()
                    continue
                elif line.split():
                    if line.split()[0].lower() == "nonbonded":
                        stat = "nonb0"
                        continue
                    if line.split()[0].lower() == "hbond":
                        stat = "hbond"
                        continue
                if stat == "bond":
                    sp = line.split()
                    bonds[min(sp[0], sp[1]) + "#" + max(sp[0], sp[1])] = line
                elif stat == "angl":
                    sp = line.split()
                    angles[min(sp[0], sp[2]) + "#" + sp[1] + "#" + max(sp[0], sp[2])] = line
                elif stat == "dihe":
                    sp = line.split()
                    b1 = sp[1] + "#" + sp[0]
                    b2 = sp[2] + "#" + sp[3]
                    dihedrals[min(b1, b2) + "##" + max(b1, b2)] = line
                elif stat == "impr":
                    sp = line.split()
                    b1 = sp[1] + "#" + sp[0]
                    b2 = sp[2] + "#" + sp[3]
                    impropers[min(b1, b2) + "##" + max(b1, b2)] = line
                elif stat == "nonb0":
                    stat = "nonb"
                elif stat == "nonb":
                    sp = line.split()
                    nonbs[sp[0]] = line
        except:
            print(line, "is not parsed")
            import sys
            sys.exit(1)
def process_inp(f):
    bonds = {}
    angles = {}
    dihedrals = {}
    impropers = {}
    nonbs = {}
    for line in open(f):
        sp = line.split()
        if sp and sp[0] == "parameters":
            add_param(sp[1], bonds, angles, dihedrals, impropers, nonbs)
    print("BONDS")
    print("\n".join(bonds.values()))
    print("ANGLES")
    print("\n".join(angles.values()))
    print("DIHEDRALS")
    print("\n".join(dihedrals.values()))
    print("IMPROPERS")
    print("\n".join(impropers.values()))
    print("NONBONDED nbxmod  5 atom cdiel fshift vatom vdistance vfswitch -")
    print("cutnb 14.0 ctofnb 12.0 ctonnb 10.0 eps 1.0 e14fac 1.0 wmin 1.5")
    print("\n".join(nonbs.values()))

import sys
process_inp(sys.argv[1])
