import sys
import re
import os
INCLUDE_RE=re.compile('#include <([^>]+)>')
def sub(m):
  hdr = m[1]
  if os.path.exists(hdr):
    return '#include "%s"' % hdr
  else:
    return m[0]
for f in sys.argv[1:]:
  newcontent = INCLUDE_RE.sub(sub, open(f).read())
  open(f, "w").write(newcontent)
  