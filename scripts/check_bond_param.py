import numpy
import pandas
import sys
import pickle
bb = pickle.load(open('bonds.pickle', 'rb'))
bbfwd = bb[bb.i < bb.j]
bbrev = bb[bb.i > bb.j]
bbrev.rename(columns={'i': 'j', 'j': 'i'})
bbrev[['dx', 'dy', 'dz']] *= -1
bb = pandas.concat([bbfwd, bbrev]).sort_values(by=['i', 'j']).reset_index(drop=True)
bb.to_pickle('bonds.pkl')
ab = pickle.load(open('angles.pickle', 'rb'))
abfwd = ab[ab.i < ab.j]
abrev = ab[ab.i > ab.j]
abrev.rename(columns={'i': 'j', 'j': 'i'})
abrev[['dx', 'dy', 'dz']] *= -1
bb = pandas.concat([bbfwd, bbrev]).sort_values(by=['i', 'j']).reset_index(drop=True)
bb.to_pickle('bonds.pkl')
tb = pickle.load(open('toris.pickle', 'rb'))
ib = pickle.load(open('imprs.pickle', 'rb'))

bt = pandas.read_csv('bond.txt', delimiter=' ', header = None)[[1, 2, 3, 4]].rename(columns=lambda x: x - 1)
bt.columns=['i', 'j', 'r0', 'kr']
bt = bt.sort_values(by=['i', 'j']).reset_index(drop=True)

bb = numpy.fromfile('.recycle/quicktest/bonds.bin', dtype=numpy.int32).reshape(-1, 3)
bb_idx = numpy.asarray([numpy.min(bb[:, :2], axis=1), numpy.max(bb[:, :2], axis=1)]).T - 1
bb_par = numpy.fromfile('.recycle/quicktest/bond_params.bin', dtype=numpy.float32).reshape(-1, 2)[bb[:, 2]]
bb = pandas.concat([pandas.DataFrame(bb_idx), pandas.DataFrame(bb_par)], axis=1)
bb.columns=['i', 'j', 'kr', 'r0']
bb = bb.sort_values(by=['i', 'j']).reset_index(drop=True)

print((bb - bt).abs().max())

at = pandas.read_csv('angle.txt', delimiter=' ', header = None)[[1, 2, 3, 4, 5, 6, 7]].rename(columns=lambda x: x - 1)
at.columns=['i', 'j', 'k', 'theta0', 'ktheta', 'r0ub', 'kub']
atmin = numpy.minimum(at.i, at.k)
atmax = numpy.maximum(at.i, at.k)
at.i = atmin
at.k = atmax
at['theta0'] = at['theta0'] / 180 * numpy.pi
at = at.sort_values(by=['i', 'j', 'k']).reset_index(drop=True)

ab = numpy.fromfile('.recycle/quicktest/angles.bin', dtype=numpy.int32).reshape(-1, 4)
ab_idx = numpy.asarray([numpy.min(ab[:, [0, 2]], axis=1), ab[:, 1], numpy.max(ab[:, [0, 2]], axis=1)]).T - 1
ab_par = numpy.fromfile('.recycle/quicktest/angle_params.bin', dtype=numpy.float32).reshape(-1, 4)[ab[:, 3]]
ab = pandas.concat([pandas.DataFrame(ab_idx), pandas.DataFrame(ab_par)], axis = 1)
ab.columns=['i', 'j', 'k', 'ktheta', 'theta0', 'kub', 'r0ub']
ab = ab.sort_values(by=['i', 'j', 'k']).reset_index(drop=True)

print((ab - at).abs().max())

dt = pandas.read_csv('dihed.txt', delimiter=' ', header = None)[[1, 2, 3, 4, 5, 6, 7]].rename(columns=lambda x: x - 1)
dt.columns=['i', 'j', 'k', 'l', 'phi0', 'kphi', 'n']
dt = dt.sort_values(by=['i', 'j', 'k', 'l', 'n']).reset_index(drop=True)
dt['phi0'] = dt['phi0'] / 180 * numpy.pi

dbin = pandas.DataFrame(numpy.fromfile('.recycle/dihed.bin').reshape(-1, 7))
dbin.columns=['i', 'j', 'k', 'l', 'kphi', 'n', 'phi0']
dbinfwd = dbin[dbin['j'] < dbin['k']]
dbinrev = dbin[dbin['k'] < dbin['j']]
dbinrev.columns = ['l', 'k', 'j', 'i', 'kphi', 'n', 'phi0']
dbin = pandas.concat([dbinfwd, dbinrev])
dbin = dbin.sort_values(by=['i', 'j', 'k', 'l', 'n']).reset_index(drop=True)
dbin[['i', 'j', 'k', 'l']]  -= 1
print((dt - dbin).abs().max())


it = pandas.read_csv('impr.txt', delimiter=' ', header = None)[[1, 2, 3, 4, 5, 6]].rename(columns=lambda x: x - 1)
it.columns=['i', 'j', 'k', 'l', 'phi0', 'kphi']
it = it.sort_values(by=['i', 'j', 'k', 'l']).reset_index(drop=True)
it['phi0'] = it['phi0'] / 180 * numpy.pi

ib = pandas.DataFrame(numpy.fromfile('.recycle/impr.bin').reshape(-1, 6))
ib.columns=['i', 'j', 'k', 'l', 'kphi', 'phi0']
ib = ib.sort_values(by=['i', 'j', 'k', 'l']).reset_index(drop=True)
ib[['i', 'j', 'k', 'l']] -= 1

print((it - ib).abs().max())
sys.exit(0)