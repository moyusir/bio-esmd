bio-ESMD
================================
bio-ESMD (biological Exa-Scale Molecular Dynamics) is a biological system simulation software for Sunway Processors. Which is build on the top of ESMD. ESMD and bio-ESMD are implemented with cell lists, which can make full use of the DMA and GLD/ST bandwidth, then we have change to optimize the computation part.

Dependencies
----
bio-ESMD uses GNU Make for compiling, on generic multi-core processors, the only dependency is MPI.

For Sunway:

#. MPI+swgcc710-5A (SWCC version has be discarded)
#. qthread (currently included in this repo, make target=5 for compile for SW5, for 7 and 9, something alike)
#. Python3 and sympy (for generating code for mSHAKE and mRATTLE)

The Makefile can detect the architecture of Sunway automatically.

Compiling
---------
With swgcc710-5A::

  module load sw/compiler/gcc710
  cd qthread
  make target=5
  cd ../src
  make

The executable will be generated in ``bin/esmd``

Run
----
bio-ESMD is released with two test cases, the command line arguments are::

  ./esmd <config file>

For example::

  ./esmd ../ubq_wb/ubq_wb_eq.conf

Suggested ``bsub`` parameters on the TaihuLight are::
  
  bsub -I -q <your_queue_name> -share_size 6144 -host_stack 16 -b -cgsp 64 -n 1 -p -sw3run swrun-5a ../bin/esmd <your_conf_file>

Currently the checkpoint is not supported.

Parameters
----
Supported parameters in config file includes:

==============  ===================================== =======================================================================
Parameter       Format                                Description 
==============  ===================================== =======================================================================
root            Path                                  Specify the path for ESMD to read file, ``$WORKDIR`` current working dir, ``$CONFDIR`` for config file dir
coordinate      Path to a PDB file                    Coordinates of the molecule
structure       Path to a TOP file                    Topology of the molecule (in GROMACS format)
parameters      Path to a parameter file              This option is deprecated
rcut            real                                  Cut-off radius
rswitch         real                                  Switch radius
coulscaling     real                                  Coulomb scaling factor for 1-4 scaling.
cell            real                                  the bounding box for the unit cell ``xlow ylow zlow xhigh yhigh zhigh``
msmlevels       int[3]                                MSM levels in three dimensions
msmorder        int                                   the MSM order
coulconst       real                                  
dt              real                                  timestep size
nsteps          int                                   number of timesteps
trajectory      Path                                  prefix for the trajectory file
trajfreq        int                                   number of timesteps between trajectory output
rigidtype       shake/lincs/none                      specify the rigid bodies constraint algorithm
rigids          string                                DISABLED
coultype        rf/shifted/msm                        Coulobm algorithm, RF for reaction field, currently eps-rf=78.5
temperature     real                                  initial temperature in K
tempcouple      nh/none                               temperature coupling algorithm, only nh or none acceptable.
tempstart       real                                  starting temperature
tempstop        real                                  ending temperature
tempchain       int                                   length of Nose-Hoover Chains
minimize        int                                   number of minimization steps.
rigidscale      real                                  We use stiff bond condition for minization with rigid bodies, that is, the bonded energy is multiplied by this number if the bond is a rigid body.
==============  ===================================== =======================================================================

Statement on Genocide Activities
================================
**Unless the developer proves that he/she has never participated in genocide activities, we will reject all commits from developers in regimes that have committed genocide (such as the genocide of Indians in the United States).**
