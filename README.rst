bio-ESMD分子动力学模拟软件
================================

bio-ESMD（biological Exa-Scale Molecular Dynamics）是一款针对神威系列处理器开发的分子动力学模拟软件。其前身是基于神威·太湖之光的分子动力学性能测试实现ESMD。与常见的分子动力学模拟软件不同，ESMD和bio-ESMD都使用了完全的格点索引实现，也就是，纯正的无邻接表实现，以在SW26010的DMA+GLD/ST框架下将其访存性能发挥到极致，从而为后续的计算优化提供机会。

**注意事项**

1. ParmEd等软件会在水的两个氢原子之间加一个键， **请手动注掉** 。
2. 如果top文件有 ``#ifdef`` ， ``#include`` 等原语，请使用 ``cpp`` 或者 ``grompp`` 等工具把它们预处理一下

依赖
----
目前bio-ESMD使用了纯的Makefile进行编译，其通用处理器版本仅依赖MPI。

对于神威版本，其依赖包括：

#. MPI+swgcc710-5A (sw5cc版本已经停止维护，请自行回滚到非键力从核刚实现的版本）
#. 基于swgcc710-5A的快速线程库qthread
#. Python3和sympy（用于生成SHAKE和RATTLE的矩阵求逆代码）

执行make时将自动依据mpicc的底层编译器选择编译通用版本还是神威版本。

编译
----
编译时需要加载swgcc710-5A::

  module load sw/compiler/gcc710
  cd qthread
  make target=5
  cd ../src
  make

可执行文件将生成在 ``bin/esmd``

运行
----
bio-ESMD软件包内内置了UBQ和6W9C两个样例输入，其命令行格式为::

  ./esmd <config file>

例如::

  ./esmd ../ubq_wb/ubq_wb_eq.conf

推荐的 ``bsub`` 参数:
  
  bsub -I -q <队列名> -share_size 6144 -host_stack 16 -b -cgsp 64 -n 1 -p -sw3run swrun-5a ../bin/esmd <配置文件>

当内存不够时推荐改大 ``share_size`` ，16的栈够用了。

目前没有restart功能。

bio-ESMD的轨迹文件后缀为 ``.estraj`` ，可以使用 ``tools/trajconv.py`` 转换成DCD格式，例如::

  python3 tools/trajconv.py ubq_wb/ubq_wb_eq.conf

轨迹转换工具会从配置文件读取元胞、输出文件路径、时间步等信息，故需要保持配置文件与运行时所使用的配置文件一致。此外，注意减少模拟所用进程数前请删除前一次模拟的轨迹，该脚本依赖NumPy。
  
参数
----
目前bio-ESMD的参数解析器比较简陋，conf文件中可以使用的字段包括:

==============  ========================  =======================================================================
参数             格式                       用途 
==============  ========================  =======================================================================
root            路径                      指定程序读取文件的路径，指定为 ``$WORKDIR`` 为当前工作目录， ``$CONFDIR`` 为conf文件所在的目录
coordinate      一个pdb文件的路径         指定分子的坐标文件
structure       一个top文件的路径         指定分子拓扑文件（GROMACS格式）
parameters      一个inp文件的路径         该选项暂时废弃
rcut            浮点数                    截断半径
rswitch         浮点数                    switch半径
coulscaling     浮点数                    1-4scaling中库仑力的系数
cell            六个浮点数                指定unit cell的低点和高点，六个数分别代表 ``xlow ylow zlow xhigh yhigh zhigh``
msmlevels       三个整数                  指定MSM方法在三个维度上的层数，最底层网格的数量为2的层数次方
msmorder        整数                      指定MSM的阶数
coulconst       浮点数                    指定库伦常数（主要是处理不同软件中库伦常数不同的情况）
dt              浮点数                    时间步，单位为飞秒
nsteps          整数                      模拟的步数
trajfreq        整数                      输出轨迹帧的频率
trajectory      路径                      输出轨迹的前缀（会被添加 ``-进程号.estraj`` ）
rigidtype       shake/lincs/none          指定刚体约束的算法，none为不进行刚体约束，这里的shake和lincs不能进行两个重原子间的刚体约束， **注意，不支持SETTLE，SETTLE自动转换为氢氧键，且通过氢原子的约束规则执行（等价于GROMACS的h-bonds）**
rigids          字符串                    暂时废弃，默认为对所有氢（当前判断方式为H开头）原子进行刚体约束
coultype        rf/shifted/msm            指定库仑力的算法，目前rf采用了默认的eps-rf=78.5，MSM暂时没有从核优化，shift采用与CHARMM相同的用法
temperature     浮点数                    初始温度，单位为K
tempcouple      nh/none                   控温算法，nh是Nose Hoover Chain算法
tempstart       浮点数                    模拟初始时控温的温度
tempstop        浮点数                    模拟结束时控温的温度，与以上参数进行线性插值获得单个时间步中控温的目标
tempchain       整数                      控温时使用的Nose Hoover Chain的长度
tempnc          整数                      每个时间步进行Nose Hoover Chain控温的次数
units           real                      目前只支持real，与LAMMPS中的units real单位对应（然而top文件仍然采用GROMACS的单位），后续可能开发metal等
minimize        整数                      进行能量最小化的步数（目前能量最小化会导致温度重新初始化一次），算法为非线性CG，使用quadratic linesearch方法寻找alpha值（暂时不稳定）
rigidscale      浮点数                    为了使能量最小化后的体系满足刚体约束的条件，这里使用了stiff bond condition，也就是说为刚体约束的键和键角乘上这个系数，从而使得能量最小化的过程中键长和键角更容易被保证（主要是我试图在minimize的时候用mSHAKE结果崩了）
==============  ========================  =======================================================================

Statement on Genocide Activities
================================
**Unless the developer proves that he/she has never participated in genocide activities, we will reject all commits from developers in regimes that have committed genocide (such as the genocide of Indians in the United States).**
