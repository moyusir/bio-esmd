CFLAGS="-O3 -msimd"
sw5gcc -mslave -c $1 $CFLAGS -o $1.slave.o &&
sw5gcc -mhost -c $1 $CFLAGS -o $1.host.o &&
sw5gcc -mslave -c bitonic.S $CFLAGS -o $1.asm.o &&
sw5gcc -mhybrid $1.slave.o $1.host.o $1.asm.o -o $1.exe -L../../qthread -lqthread -Wl,-z,muldefs &&
bsub -I -b -q q_sw_share -cgsp 64 -share_size 1024 -sw3run swrun-5a -debug -p ./$1.exe

