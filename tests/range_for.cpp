#include <typeinfo>
#include <cstdio>
#include <tuple>
struct test{
  struct test_iterator{
    int i;
    const int &operator *(){
      return i;
    }
    test_iterator &operator ++(){
      i += 2;
      return *this;
    }
    // bool operator==(const test_iterator &o){
    //   return i == o.i;
    // }
    bool operator<(const test_iterator &o) const {
      return this->i < o.i;
    }
  };
  test_iterator begin(){
    return test_iterator{0};
  }
  test_iterator end(){
    return test_iterator{10};
  }
  // int val[5] = {0, 1, 2, 3, 4};
  // int *begin(){
  //   return val;
  // }
  // int *end(){
  //   return val + 5;
  // }
};
template <typename T>
struct loop_less{
  T &ref;
  loop_less(T &t) : ref(t){};
  template<typename E>
  struct less_ite {
    E ref;
    less_ite(E e) : ref(e){};
    bool operator!=(const less_ite<E> &o) const{
      return ref < o.ref;
    }
    less_ite<E> &operator++(){
      ++ref;
      return *this;
    }
    auto &operator *(){
      return *ref;
    }
  };
  auto begin(){
    return less_ite(ref.begin());
  }
  auto end(){
    return less_ite(ref.end());
  }
};
struct tt {
  std::tuple<int, int> val[5] = {std::make_tuple(1, 1), std::make_tuple(2, 2), std::make_tuple(3, 3), std::make_tuple(4, 4), std::make_tuple(5, 5)};
  std::tuple<int, int> *begin(){
    return val;
  }
  std::tuple<int, int> *end(){
    return val + 5;
  }
};
int main(){
  test t;

  for (auto &x: loop_less(t)) {
    printf("%d\n", x);
  }
  // tt t2;
  // for (auto &[x, y] : t2){
  //   printf("%d %d\n", x, y);
  // }
}
