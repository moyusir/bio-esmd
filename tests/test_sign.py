
import sys
from clang.cindex import Config
from clang.cindex import Index
from clang.cindex import TranslationUnit

#Config.set_library_path("/usr/local/Cellar/llvm/3.9.0/lib")

def walk_recursive(cursor, depth, output):
    if output:
        print("%s : %s" % ("  " * depth + cursor.kind.name, cursor.displayname))
    for child in cursor.get_children():
        output_child = True #("SDL" in child.displayname) or (depth > 0 and output == True)
        walk_recursive(child, depth+1, output_child)

args = ["-Wall", "-I.", "-I/usr/lib/clang/10.0.0/include/"]
opts = TranslationUnit.PARSE_INCOMPLETE | TranslationUnit.PARSE_SKIP_FUNCTION_BODIES # a bitwise or of TranslationUnit.PARSE_XXX flags.

idx = Index.create()                           # Index : the primary interface to the Clang CIndex library.
tu  = idx.parse(sys.argv[1], args, None, opts) # TranslationUnit : represents a source code translation unit.
walk_recursive(tu.cursor, 0, True)      