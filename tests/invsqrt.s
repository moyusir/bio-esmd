	.set noreorder
	.set volatile
	.set noat
	.set nomacro
	.arch sw3
### Optimization level and target	: -O3 -mslave 
	.section .text1, "ax"
	.align 2
	.align 4
	.globl slave__Z4rpccRd
	.ent slave__Z4rpccRd
$slave__Z4rpccRd..ng:
slave__Z4rpccRd:
	.frame $30,0,$26,0
$LFB126:
	.cfi_startproc
	.prologue 0
	fldd $1,0($16)
	.set	macro
 # 6 "invsqrt.cpp" 1
	rcsr $0, 4
	
 # 0 "" 2
	.set	nomacro
	fstd $1,0($16)
	ret $31,($26),1
	.cfi_endproc
$LFE126:
	.end slave__Z4rpccRd
	.section	.rodata.str1.1,"aMS",@progbits,1
$LC0:
	.string	"%f %ld\n"
	.section	.text1.startup,"ax",@progbits
	.align 2
	.align 4
	.globl main
	.ent main
main:
	.frame $30,64,$26,0
	.mask 0x4000000,-64
$LFB128:
	.cfi_startproc
	ldih $29,0($27)		!gpdisp!1
	ldi $29,0($29)		!gpdisp!1
$main..ng:
	ldi $30,-64($30)
	.cfi_def_cfa_offset 64
	ldl $27,atof($29)		!literal!2
	stl $26,0($30)
	.cfi_offset 26, -64
	.prologue 1
	ldl $16,8($17)
	call $26,($27),atof		!lituse_jsr!2
	ldih $29,0($26)		!gpdisp!3
	ldi $29,0($29)		!gpdisp!3
	mov $31,$17
	.set	macro
 # 6 "invsqrt.cpp" 1
	rcsr $1, 4
	
 # 0 "" 2
	.set	nomacro
	stl $1,40($30)
	.set	macro
 # 31 "invsqrt.cpp" 1
	ldl  $1, MAGIC_NUM($31)!ldmlow
	srl  $0, 1, $17
	ldl  $2, f1_5($31)!ldmlow
	subl $1, $17, $17
	fmas $0, $2, $0, $3
	ldi  $0, 0($2)fmuld $17, $0, $1
	fmuld $17, $17, $2
	fmuld $17, $3, $17
	fnmad $17, $2, $1, $17
	fmuld $17, $0, $1
	fmuld $17, $17, $2
	fmuld $17, $3, $17
	fnmad $17, $2, $1, $17
	fmuld $17, $0, $1
	fmuld $17, $17, $2
	fmuld $17, $3, $17
	fnmad $17, $2, $1, $17
	
 # 0 "" 2
	.set	nomacro
	fcvtld $17,$17
	ldih $16,$LC0($29)		!gprelhigh
	ldl $27,printf($29)		!literal!4
	.set	macro
 # 6 "invsqrt.cpp" 1
	rcsr $1, 4
	
 # 0 "" 2
	.set	nomacro
	stl $1,32($30)
	ldi $16,$LC0($16)		!gprellow
	ldl $18,32($30)
	ldl $1,40($30)
	subl $18,$1,$18
	call $26,($27),printf		!lituse_jsr!4
	ldih $29,0($26)		!gpdisp!5
	ldl $26,0($30)
	mov $31,$0
	ldi $29,0($29)		!gpdisp!5
	ldi $30,64($30)
	.cfi_restore 26
	.cfi_def_cfa_offset 0
	ret $31,($26),1
	.cfi_endproc
$LFE128:
	.end main
	.ident	"GCC: (GNU) 7.1.0 20170502 (sw5gcc-87 Committer: GuanML <gml@163.com> by comp5 at localhost.localdomain on Mon Aug 10 14:34:03 CST 2020)"
	.section	.note.GNU-stack,"",@progbits
