#define BITONIC_VEC_INIT_8(v0, v1)                                             \
  {                                                                            \
    int256 t0, t1;                                                             \
    asm("vshff   %[V0], %[V0], 0xb1, %[VSWAP]\n\t"                             \
        "vsubl   %[V0], %[VSWAP], %[VCMP]\n\t"                                 \
        "vlog3r0 %[AUX], %[VFLIP6], %[VCMP], %[VCMP]\n\t"                      \
        "vsellt  %[VCMP], %[V0], %[VSWAP], %[V0]\n\t"                          \
        "vshff   %[V1], %[V1], 0xb1, %[VSWAP]\n\t"                             \
        "vsubl   %[V1], %[VSWAP], %[VCMP]\n\t"                                 \
        "vlog3r0 %[AUX], %[VFLIP6], %[VCMP], %[VCMP]\n\t"                      \
        "vsellt  %[VCMP], %[V1], %[VSWAP], %[V1]\n\t"                          \
        "vshff   %[V0], %[V0], 0x4e, %[VSWAP]\n\t"                             \
        "vsubl   %[V0], %[VSWAP], %[VCMP]\n\t"                                 \
        "vlog3r0 %[AUX], %[VFLIP3], %[VCMP], %[VCMP]\n\t"                      \
        "vsellt  %[VCMP], %[VSWAP], %[V0], %[V0]\n\t"                          \
        "vshff   %[V1], %[V1], 0x4e, %[VSWAP]\n\t"                             \
        "vsubl   %[V1], %[VSWAP], %[VCMP]\n\t"                                 \
        "vlog3r0 %[AUX], %[VFLIP3], %[VCMP], %[VCMP]\n\t"                      \
        "vsellt  %[VCMP], %[V1], %[VSWAP], %[V1]\n\t"                          \
        "vshff   %[V0], %[V0], 0xb1, %[VSWAP]\n\t"                             \
        "vsubl   %[V0], %[VSWAP], %[VCMP]\n\t"                                 \
        "vlog3r0 %[AUX], %[VFLIP5], %[VCMP], %[VCMP]\n\t"                      \
        "vsellt  %[VCMP], %[VSWAP], %[V0], %[V0]\n\t"                          \
        "vshff   %[V1], %[V1], 0xb1, %[VSWAP]\n\t"                             \
        "vsubl   %[V1], %[VSWAP], %[VCMP]\n\t"                                 \
        "vlog3r0 %[AUX], %[VFLIP5], %[VCMP], %[VCMP]\n\t"                      \
        "vsellt  %[VCMP], %[V1], %[VSWAP], %[V1]\n\t"                          \
        : [ V0 ] "=r"(v0), [ V1 ] "=r"(v1), [ VSWAP ] "=&r"(t0),               \
          [ VCMP ] "=&r"(t1)                                                   \
        : [ AUX ] "r"(aux), [ VFLIP3 ] "r"(flip3), [ VFLIP6 ] "r"(flip6),      \
          [ VFLIP5 ] "r"(flip5), [ R0 ] "0"(v0), [ R1 ] "1"(v1));              \
  }
//vlog3r0 AUX, VFLIP3, VCMP, VCMP->
#define BITONIC_INC_8(v0, v1)                                                  \
  {                                                                            \
    int256 t0, t1;                                                             \
    asm("vsubl   %[V0], %[V1], %[VCMP]\n\t"                                    \
        "vcpys   %[VCMP], %[VFLIP3], %[VCMP]\n\t"                              \
        "vsellt  %[VCMP], %[V1], %[V0], %[VSWAP]\n\t"                          \
        "vsellt  %[VCMP], %[V0], %[V1], %[V0]\n\t"                             \
        "vcpys   %[VSWAP], %[VSWAP], %[V1]\n\t"                                \
        "vshff   %[V0], %[V0], 0x4e, %[VSWAP]\n\t"                             \
        "vsubl   %[V0], %[VSWAP], %[VCMP]\n\t"                                 \
        "vlog3r0 %[AUX], %[VFLIP3], %[VCMP], %[VCMP]\n\t"                      \
        "vsellt  %[VCMP], %[VSWAP], %[V0], %[V0]\n\t"                          \
        "vshff   %[V1], %[V1], 0x4e, %[VSWAP]\n\t"                             \
        "vsubl   %[V1], %[VSWAP], %[VCMP]\n\t"                                 \
        "vlog3r0 %[AUX], %[VFLIP3], %[VCMP], %[VCMP]\n\t"                      \
        "vsellt  %[VCMP], %[VSWAP], %[V1], %[V1]\n\t"                          \
        "vshff   %[V0], %[V0], 0xb1, %[VSWAP]\n\t"                             \
        "vsubl   %[V0], %[VSWAP], %[VCMP]\n\t"                                 \
        "vlog3r0 %[AUX], %[VFLIP5], %[VCMP], %[VCMP]\n\t"                      \
        "vsellt  %[VCMP], %[VSWAP], %[V0], %[V0]\n\t"                          \
        "vshff   %[V1], %[V1], 0xb1, %[VSWAP]\n\t"                             \
        "vsubl   %[V1], %[VSWAP], %[VCMP]\n\t"                                 \
        "vlog3r0 %[AUX], %[VFLIP5], %[VCMP], %[VCMP]\n\t"                      \
        "vsellt  %[VCMP], %[VSWAP], %[V1], %[V1]\n\t"                          \
        : [ V0 ] "=r"(v0), [ V1 ] "=r"(v1), [ VSWAP ] "=&r"(t0),               \
          [ VCMP ] "=&r"(t1)                                                   \
        : [ AUX ] "r"(aux), [ VFLIP3 ] "r"(flip3), [ VFLIP5 ] "r"(flip5),      \
          [ R0 ] "0"(v0), [ R1 ] "1"(v1));                                     \
  }

#define BITONIC_DEC_8(v0, v1)                                                  \
  {                                                                            \
    int256 t0, t1;                                                             \
    asm("vsubl   %[V0], %[V1], %[VCMP]\n\t"                                    \
        "vcpys   %[VCMP], %[VFLIP3], %[VCMP]\n\t"                              \
        "vsellt  %[VCMP], %[V0], %[V1], %[VSWAP]\n\t"                          \
        "vsellt  %[VCMP], %[V1], %[V0], %[V0]\n\t"                             \
        "vcpys   %[VSWAP], %[VSWAP], %[V1]\n\t"                                \
        "vshff   %[V0], %[V0], 0x4e, %[VSWAP]\n\t"                             \
        "vsubl   %[V0], %[VSWAP], %[VCMP]\n\t"                                 \
        "vlog3r0 %[AUX], %[VFLIP3], %[VCMP], %[VCMP]\n\t"                      \
        "vsellt  %[VCMP], %[V0], %[VSWAP], %[V0]\n\t"                          \
        "vshff   %[V1], %[V1], 0x4e, %[VSWAP]\n\t"                             \
        "vsubl   %[V1], %[VSWAP], %[VCMP]\n\t"                                 \
        "vlog3r0 %[AUX], %[VFLIP3], %[VCMP], %[VCMP]\n\t"                      \
        "vsellt  %[VCMP], %[V1], %[VSWAP], %[V1]\n\t"                          \
        "vshff   %[V0], %[V0], 0xb1, %[VSWAP]\n\t"                             \
        "vsubl   %[V0], %[VSWAP], %[VCMP]\n\t"                                 \
        "vlog3r0 %[AUX], %[VFLIP5], %[VCMP], %[VCMP]\n\t"                      \
        "vsellt  %[VCMP], %[V0], %[VSWAP], %[V0]\n\t"                          \
        "vshff   %[V1], %[V1], 0xb1, %[VSWAP]\n\t"                             \
        "vsubl   %[V1], %[VSWAP], %[VCMP]\n\t"                                 \
        "vlog3r0 %[AUX], %[VFLIP5], %[VCMP], %[VCMP]\n\t"                      \
        "vsellt  %[VCMP], %[V1], %[VSWAP], %[V1]\n\t"                          \
        : [ V0 ] "=r"(v0), [ V1 ] "=r"(v1), [ VSWAP ] "=&r"(t0),               \
          [ VCMP ] "=&r"(t1)                                                   \
        : [ AUX ] "r"(aux), [ VFLIP3 ] "r"(flip3), [ VFLIP5 ] "r"(flip5),      \
          [ R0 ] "0"(v0), [ R1 ] "1"(v1));                                     \
  }

#define BITONIC_INC_4V(v0, v1, v2, v3)                                         \
  {                                                                            \
    int256 t0, t1;                                                             \
    asm("vsubl   %[V0], %[V2], %[VCMP]\n\t"                                    \
        "vcpys   %[VCMP], %[VFLIP3], %[VCMP]\n\t"                              \
        "vsellt  %[VCMP], %[V2], %[V0], %[VSWAP]\n\t"                          \
        "vsellt  %[VCMP], %[V0], %[V2], %[V0]\n\t"                             \
        "vcpys   %[VSWAP], %[VSWAP], %[V2]\n\t"                                \
        "vsubl   %[V1], %[V3], %[VCMP]\n\t"                                    \
        "vcpys   %[VCMP], %[VFLIP3], %[VCMP]\n\t"                              \
        "vsellt  %[VCMP], %[V3], %[V1], %[VSWAP]\n\t"                          \
        "vsellt  %[VCMP], %[V1], %[V3], %[V1]\n\t"                             \
        "vcpys   %[VSWAP], %[VSWAP], %[V3]\n\t"                                \
        : [ V0 ] "=r"(v0), [ V1 ] "=r"(v1), [ V2 ] "=r"(v2), [ V3 ] "=r"(v3),  \
          [ VSWAP ] "=&r"(t0), [ VCMP ] "=&r"(t1)                              \
        : [ AUX ] "r"(aux), [ VFLIP3 ] "r"(flip3), [ R0 ] "0"(v0),             \
          [ R1 ] "1"(v1), [ R2 ] "2"(v2), [ R3 ] "3"(v3));                     \
  }

#define BITONIC_DEC_4V(v0, v1, v2, v3)                                         \
  {                                                                            \
    int256 t0, t1;                                                             \
    asm("vsubl   %[V0], %[V2], %[VCMP]\n\t"                                    \
        "vcpys   %[VCMP], %[VFLIP3], %[VCMP]\n\t"                              \
        "vsellt  %[VCMP], %[V0], %[V2], %[VSWAP]\n\t"                          \
        "vsellt  %[VCMP], %[V2], %[V0], %[V0]\n\t"                             \
        "vcpys   %[VSWAP], %[VSWAP], %[V2]\n\t"                                \
        "vsubl   %[V1], %[V3], %[VCMP]\n\t"                                    \
        "vcpys   %[VCMP], %[VFLIP3], %[VCMP]\n\t"                              \
        "vsellt  %[VCMP], %[V1], %[V3], %[VSWAP]\n\t"                          \
        "vsellt  %[VCMP], %[V3], %[V1], %[V1]\n\t"                             \
        "vcpys   %[VSWAP], %[VSWAP], %[V3]\n\t"                                \
        : [ V0 ] "=r"(v0), [ V1 ] "=r"(v1), [ V2 ] "=r"(v2), [ V3 ] "=r"(v3),  \
          [ VSWAP ] "=&r"(t0), [ VCMP ] "=&r"(t1)                              \
        : [ AUX ] "r"(aux), [ VFLIP3 ] "r"(flip3), [ R0 ] "0"(v0),             \
          [ R1 ] "1"(v1), [ R2 ] "2"(v2), [ R3 ] "3"(v3));                     \
  }
#define AUX_FLIP_DEF                                                           \
  int256 flip0 =                                                               \
      simd_set_int256(0x3ffL << 52, 0x3ffL << 52, 0x3ffL << 52, 0x3ffL << 52); \
  int256 flip1 =                                                               \
      simd_set_int256(0xbffL << 52, 0x3ffL << 52, 0x3ffL << 52, 0x3ffL << 52); \
  int256 flip2 =                                                               \
      simd_set_int256(0x3ffL << 52, 0xbffL << 52, 0x3ffL << 52, 0x3ffL << 52); \
  int256 flip3 =                                                               \
      simd_set_int256(0xbffL << 52, 0xbffL << 52, 0x3ffL << 52, 0x3ffL << 52); \
  int256 flip4 =                                                               \
      simd_set_int256(0x3ffL << 52, 0x3ffL << 52, 0xbffL << 52, 0x3ffL << 52); \
  int256 flip5 =                                                               \
      simd_set_int256(0xbffL << 52, 0x3ffL << 52, 0xbffL << 52, 0x3ffL << 52); \
  int256 flip6 =                                                               \
      simd_set_int256(0x3ffL << 52, 0xbffL << 52, 0xbffL << 52, 0x3ffL << 52); \
  int256 flip7 =                                                               \
      simd_set_int256(0xbffL << 52, 0xbffL << 52, 0xbffL << 52, 0x3ffL << 52); \
  int256 aux = simd_set_int256(1L << 63, 1L << 63, 1L << 63, 1L << 63);        \
  asm("wcsr %0, 0x80\n\t" ::"r"(0x6c));
