#include <simd.h>
int main(){
  doublev4 a = 2.0;
  doublev4 b = __builtin_sw_vsrld(a, 1);
  simd_print_doublev4(b);
}
