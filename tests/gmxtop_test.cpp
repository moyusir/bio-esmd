#include "gmxtop.hpp"
int main(){
  gmxtop top;
  FILE *topfile = fopen("../ubq_wb/ubq_wb.top", "r");
  top.parse_file(topfile);
  top.unitconv, unit lunit, unit runit)
  listed_force<harmonic_bond_param> bonds;
  param_mapper<harmonic_bond_param> bonds_mapper("bonds mapper");
  top.export_listed<gmxtop::KEY_BOND>(bonds, bonds_mapper, 1<<1);
  printf("#bonds=%lu\n", bonds.topo.size());
  listed_force<urey_bradly_param> angles;
  param_mapper<urey_bradly_param> angles_mapper("angles mapper");
  top.export_listed<gmxtop::KEY_ANGLE>(angles, angles_mapper, 1<<5);
  printf("#angles=%lu\n", angles.topo.size());
  listed_force<cosine_torsion_param> tors;
  param_mapper<cosine_torsion_param> tors_mapper("tors mapper");
  top.export_listed<gmxtop::KEY_DIHEDRAL>(tors, tors_mapper, 1<<9);
  printf("#tors=%lu\n", tors.topo.size());

}
