#include <utility>
#include <tuple>
template<int I, int FO>
struct filter_out{
  typedef std::integer_sequence<int, I> out_seq;
};
template<int I>
struct filter_out<I, I>{
  typedef std::integer_sequence<int> out_seq;
};
template<int I, int F>
using filter_out_one = typename filter_out<I, F>::out_seq;

template<int ...Is>
auto merge_seq(std::integer_sequence<int, Is...> seq0) {
  return seq0;
}
template<int ...Is, int ...Js, typename ...Ts>
auto merge_seq(std::integer_sequence<int, Is...> seq0, std::integer_sequence<int, Js...> seq1, Ts ...rest) {
  auto ijseq = std::integer_sequence<int, Is..., Js...>();
  return merge_seq(ijseq, rest...);
}
template<int F, int ...Is>
auto filter_out_seq(std::integer_sequence<int, Is...> seq) {
  return merge_seq(filter_out_one<Is, F>{}...);
}
int main(){
  // auto iseq = std::make_integer_sequence<int, 2>();
  // auto jseq = filter_out_seq<jseq, 
  auto a = filter_out_one<0, 1>{};
  auto kseq = filter_out_seq<0>(std::integer_sequence<int, 0, 1, 2>());
}