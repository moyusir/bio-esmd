
// #define YAML_ROOT                                                     \
//   YAML_ROOT_MAPPING(charmm_args,                                      \
//                     YAML_STR(coordinate);                             \
//                     YAML_STR(structure);                              \
//                     YAML_STR(parameter);                              \
//                     YAML_RVEC(origin);                                \
//                     YAML_RVEC(basis);                                 \
//                     YAML_MAPPING(nonbprops,                           \
//                                  YAML_STR(exclude);                   \
//                                  YAML_REAL(scale14);                  \
//                                  YAML_REAL(cutoff);                   \
//                                  YAML_REAL(skin);                     \
//                                  YAML_STR(vdwstyle);                  \
//                                  YAML_MAPPING(vdwprops,               \
//                                               YAML_REAL(switchdist)); \
//                                  YAML_STR(coulstyle);                 \
//                                  YAML_MAPPING(coulprops,              \
//                                               YAML_IVEC(levels);      \
//                                               YAML_INT(order))));
// #include <esmd_yaml_map.h>
// int main() {
//   memory_init();
//   yaml_context_t *ctx = yaml_open("ubq_wb/ubq_wb_eq.yaml");
//   struct charmm_args args;
//   parse_charmm_args(&args, ctx);
//   memory_print();
//   return 0;
// }