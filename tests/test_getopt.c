#define ESMD_DESC "Parse and do CHARMM argument initialization"
#define ESMD_OPTS                                                 \
  OPT(verbose, NOARG, v, int, set1, 0, "be verbose")              \
  OPT(top, REARG, t, char *, strdup, NULL, "toplogy file")      \
  OPT(coordinate, REARG, f, char *, strdup, NULL, "coordinate") \
  OPT(parameter, REARG, p, char *, strdup, NULL, "parameter")   \
  OPT(exclude, REARG, e, char *, strdup, NULL, "exclude")       \
  POSOPT(pos1, int, atoi, 0, "c1")                      \
  POSOPT(pos2, float, (float)atof, 0, "c2")
#define ESMD_NAME test_getopt
#include <esmd_getopt.h>
//#define TEST_GETOPT
#ifdef TEST_GETOPT
int main(int argc, char **argv) {
  test_getopt_opts_t opt;
  test_getopt_print_help(stderr);
  test_getopt_parse_args(&opt, argc, argv);
  return 0;
}
#endif