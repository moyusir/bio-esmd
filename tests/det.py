def dettrace(rmask, cmask, ind = 0):
  if sum(rmask) != 0:
    print(" " * ind, "".join(map(str, rmask)), "".join(map(str,cmask)))
  for i in range(len(rmask)):
    if rmask[i] == 1:
      rmask[i] = 0
      for j in range(len(cmask)):
        if cmask[j] == 1:
          cmask[j] = 0
          dettrace(rmask, cmask, ind+1)
          cmask[j] = 1
      rmask[i] = 1
      return
dettrace([1,1,1,1,1], [1,1,1,1,1])