import re
import sys
TITLE_RE = re.compile(r'(\d+) !N(.*)')

for line in open(sys.argv[1]):
  if TITLE_RE.search(line):
    print(TITLE_RE.search(line).groups())
