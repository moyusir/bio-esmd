
INLINE size_t memtran(void *dest, void *src, size_t size) {
  memcpy(dest, src, size);
  return size;
}
#define pack_field(ptr, field, n) (ptr) += memtran((ptr), (field), sizeof(*(field)) * (n))
#define unpack_field(ptr, field, n) (ptr) += memtran((field), (ptr), sizeof(*(field)) * (n))
static int stag_next = 0x3001;
static int stag_prev = 0x3000;
static int rtag_next = 0x3000;
static int rtag_prev = 0x3001;
#define COMM_ISEND(mpp, size, dir, axis) \
  MPI_Isend((mpp)->send_##dir, (size), MPI_BYTE, (mpp)->dir.axis, stag_##dir, (mpp)->comm, &(mpp)->send_req_##dir)
#define COMM_IRECV(mpp, dir, axis) \
  MPI_Irecv((mpp)->recv_##dir, (mpp)->max_comm_size, MPI_BYTE, (mpp)->dir.axis, rtag_##dir, (mpp)->comm, &(mpp)->recv_req_##dir)
#define COMM_WAITALL(mpp)                                        \
  {                                                              \
    MPI_Wait(&((mpp)->send_req_prev), &((mpp)->send_stat_prev)); \
    MPI_Wait(&((mpp)->send_req_next), &((mpp)->send_stat_next)); \
    MPI_Wait(&((mpp)->recv_req_prev), &((mpp)->recv_stat_prev)); \
    MPI_Wait(&((mpp)->recv_req_next), &((mpp)->recv_stat_next)); \
  }

size_t pack_cell_forward_most(void *buf, celldata_t *cell){
  *(long*)buf = cell->natom;
  void *ptr = buf + 8;
  pack_field(ptr, cell->tag, cell->natom);
  pack_field(ptr, cell->x, cell->natom);
  pack_field(ptr, cell->q, cell->natom);
  pack_field(ptr, cell->t, cell->natom);
  pack_field(ptr, cell->mass, cell->natom);
  pack_field(ptr, cell->rmass, cell->natom);
  return ptr - buf;
}
//['tag', 'x', 'q', 't', 'mass', 'rmass']
size_t unpack_cell_forward_most(void *buf, celldata_t *cell){
  cell->natom = *(long*)buf;
  void *ptr = buf + 8;
  unpack_field(ptr, cell->tag, cell->natom);
  unpack_field(ptr, cell->x, cell->natom);
  unpack_field(ptr, cell->q, cell->natom);
  unpack_field(ptr, cell->t, cell->natom);
  unpack_field(ptr, cell->mass, cell->natom);
  unpack_field(ptr, cell->rmass, cell->natom);
  return ptr - buf;
}

size_t pack_brick_forward_most(void *buf, cellgrid_t *grid, int xlo, int xhi, int ylo, int yhi, int zlo, int zhi) {
  void *ptr = buf;
  for (int i = xlo; i < xhi; i ++) {
    for (int j = ylo; j < yhi; j ++) {
      for (int k = zlo; k < zhi; k ++) {
        celldata_t *cell = get_cell_xyz(grid, i, j, k);
        ptr += pack_cell_forward_most(ptr, cell);
      }
    }
  }
  return ptr - buf;
}

size_t unpack_brick_forward_most(void *buf, cellgrid_t *grid, int xlo, int xhi, int ylo, int yhi, int zlo, int zhi) {
  void *ptr = buf;
  for (int i = xlo; i < xhi; i ++) {
    for (int j = ylo; j < yhi; j ++) {
      for (int k = zlo; k < zhi; k ++) {
        celldata_t *cell = get_cell_xyz(grid, i, j, k);
        ptr += unpack_cell_forward_most(ptr, cell);
      }
    }
  }
  return ptr - buf;
}

void forward_comm_most(cellgrid_t *grid, mpp_t *mpp) {
  vec<int> *lo = &(grid->dim.lo);
  vec<int> *hi = &(grid->dim.hi);
  vec<int> *nlocal = &(grid->nlocal);
  int nn = grid->nn;

  size_t nsend_prev, nsend_next;

  COMM_IRECV(mpp, prev, z);
  COMM_IRECV(mpp, next, z);
  nsend_prev = pack_brick_forward_most(mpp->send_prev, grid, 0, nlocal->x, 0, nlocal->y, 0, nn);
  nsend_next = pack_brick_forward_most(mpp->send_next, grid, 0, nlocal->x, 0, nlocal->y, nlocal->z - nn, nlocal->z);
  COMM_ISEND(mpp, nsend_prev, prev, z);
  COMM_ISEND(mpp, nsend_next, next, z);
  COMM_WAITALL(mpp);
  unpack_brick_forward_most(mpp->recv_prev, grid, 0, nlocal->x, 0, nlocal->y, lo->z, lo->z + nn);
  unpack_brick_forward_most(mpp->recv_next, grid, 0, nlocal->x, 0, nlocal->y, hi->z - nn, hi->z);

  COMM_IRECV(mpp, prev, y);
  COMM_IRECV(mpp, next, y);
  nsend_prev = pack_brick_forward_most(mpp->send_prev, grid, 0, nlocal->x, 0, nn, lo->z, hi->z);
  nsend_next = pack_brick_forward_most(mpp->send_next, grid, 0, nlocal->x, nlocal->y - nn, nlocal->y, lo->z, hi->z);
  COMM_ISEND(mpp, nsend_prev, prev, y);
  COMM_ISEND(mpp, nsend_next, next, y);
  COMM_WAITALL(mpp);
  unpack_brick_forward_most(mpp->recv_prev, grid, 0, nlocal->x, lo->y, lo->y + nn, lo->z, hi->z);
  unpack_brick_forward_most(mpp->recv_next, grid, 0, nlocal->x, hi->y - nn, hi->y, lo->z, hi->z);

  COMM_IRECV(mpp, prev, x);
  COMM_IRECV(mpp, next, x);
  nsend_prev = pack_brick_forward_most(mpp->send_prev, grid, 0, nn, lo->y, hi->y, lo->z, hi->z);
  nsend_next = pack_brick_forward_most(mpp->send_next, grid, nlocal->x - nn, nlocal->x, lo->y, hi->y, lo->z, hi->z);
  COMM_ISEND(mpp, nsend_prev, prev, x);
  COMM_ISEND(mpp, nsend_next, next, x);
  COMM_WAITALL(mpp);
  unpack_brick_forward_most(mpp->recv_prev, grid, lo->x, lo->x + nn, lo->y, hi->y, lo->z, hi->z);
  unpack_brick_forward_most(mpp->recv_next, grid, hi->x - nn, hi->x, lo->y, hi->y, lo->z, hi->z);
}

size_t pack_cell_forward_x(void *buf, celldata_t *cell){
  void *ptr = buf;
  pack_field(ptr, cell->x, cell->natom);
  return ptr - buf;
}
//['x']
size_t unpack_cell_forward_x(void *buf, celldata_t *cell){
  void *ptr = buf;
  unpack_field(ptr, cell->x, cell->natom);
  return ptr - buf;
}

size_t pack_brick_forward_x(void *buf, cellgrid_t *grid, int xlo, int xhi, int ylo, int yhi, int zlo, int zhi) {
#ifdef __sw__
  return pack_brick_sw_forward_x(buf, grid, xlo, xhi, ylo, yhi, zlo, zhi);
#endif
  void *ptr = buf;
  for (int i = xlo; i < xhi; i ++) {
    for (int j = ylo; j < yhi; j ++) {
      for (int k = zlo; k < zhi; k ++) {
        celldata_t *cell = get_cell_xyz(grid, i, j, k);
        ptr += pack_cell_forward_x(ptr, cell);
      }
    }
  }
  return ptr - buf;
}

size_t unpack_brick_forward_x(void *buf, cellgrid_t *grid, int xlo, int xhi, int ylo, int yhi, int zlo, int zhi) {
#ifdef __sw__
  return unpack_brick_sw_forward_x(buf, grid, xlo, xhi, ylo, yhi, zlo, zhi);
#endif
  void *ptr = buf;
  for (int i = xlo; i < xhi; i ++) {
    for (int j = ylo; j < yhi; j ++) {
      for (int k = zlo; k < zhi; k ++) {
        celldata_t *cell = get_cell_xyz(grid, i, j, k);
        ptr += unpack_cell_forward_x(ptr, cell);
      }
    }
  }
  return ptr - buf;
}
#include "timer.h"
DEF_TIMER(PACKX, "comm/packx")
DEF_TIMER(UNPACKX, "comm/unpackx")
#ifdef __sw__
#include "sw/swarch.h"
#endif
void forward_comm_x(cellgrid_t *grid, mpp_t *mpp) {
  vec<int> *lo = &(grid->dim.lo);
  vec<int> *hi = &(grid->dim.hi);
  vec<int> *nlocal = &(grid->nlocal);
  int nn = grid->nn;
  #ifdef __sw__
  sw_archdata_t *archdata = grid->arch_data;
  #endif
  size_t nsend_prev, nsend_next;

  COMM_IRECV(mpp, prev, z);
  COMM_IRECV(mpp, next, z);
  timer_start(PACKX);
  #ifdef __sw__
  pack_brick_pre_sw_forward_x(mpp->send_prev, grid, archdata->pack_params + PACK_FWD_NEG_Z);
  pack_brick_pre_sw_forward_x(mpp->send_next, grid, archdata->pack_params + PACK_FWD_POS_Z);
  #else
  nsend_prev = pack_brick_forward_x(mpp->send_prev, grid, 0, nlocal->x, 0, nlocal->y, 0, nn);
  nsend_next = pack_brick_forward_x(mpp->send_next, grid, 0, nlocal->x, 0, nlocal->y, nlocal->z - nn, nlocal->z);
  #endif
  timer_stop(PACKX);
  COMM_ISEND(mpp, nsend_prev, prev, z);
  COMM_ISEND(mpp, nsend_next, next, z);
  COMM_WAITALL(mpp);
  timer_start(UNPACKX);
  #ifdef __sw__
  unpack_brick_pre_sw_forward_x(mpp->recv_prev, grid, archdata->pack_params + UNPACK_FWD_NEG_Z);
  unpack_brick_pre_sw_forward_x(mpp->recv_next, grid, archdata->pack_params + UNPACK_FWD_POS_Z);
  #else
  unpack_brick_forward_x(mpp->recv_prev, grid, 0, nlocal->x, 0, nlocal->y, lo->z, lo->z + nn);
  unpack_brick_forward_x(mpp->recv_next, grid, 0, nlocal->x, 0, nlocal->y, hi->z - nn, hi->z);
  #endif
  timer_stop(UNPACKX);
  COMM_IRECV(mpp, prev, y);
  COMM_IRECV(mpp, next, y);
  timer_start(PACKX);
  #ifdef __sw__
  pack_brick_pre_sw_forward_x(mpp->send_prev, grid, archdata->pack_params + PACK_FWD_NEG_Y);
  pack_brick_pre_sw_forward_x(mpp->send_next, grid, archdata->pack_params + PACK_FWD_POS_Y);
  #else
  nsend_prev = pack_brick_forward_x(mpp->send_prev, grid, 0, nlocal->x, 0, nn, lo->z, hi->z);
  nsend_next = pack_brick_forward_x(mpp->send_next, grid, 0, nlocal->x, nlocal->y - nn, nlocal->y, lo->z, hi->z);
  #endif
  timer_stop(PACKX);
  COMM_ISEND(mpp, nsend_prev, prev, y);
  COMM_ISEND(mpp, nsend_next, next, y);
  COMM_WAITALL(mpp);
  timer_start(UNPACKX);
  #ifdef __sw__
  unpack_brick_pre_sw_forward_x(mpp->recv_prev, grid, archdata->pack_params + UNPACK_FWD_NEG_Z);
  unpack_brick_pre_sw_forward_x(mpp->recv_next, grid, archdata->pack_params + UNPACK_FWD_POS_Z);
  #else
  unpack_brick_forward_x(mpp->recv_prev, grid, 0, nlocal->x, lo->y, lo->y + nn, lo->z, hi->z);
  unpack_brick_forward_x(mpp->recv_next, grid, 0, nlocal->x, hi->y - nn, hi->y, lo->z, hi->z);
  #endif
  timer_stop(UNPACKX);
  COMM_IRECV(mpp, prev, x);
  COMM_IRECV(mpp, next, x);
  timer_start(PACKX);
  #ifdef __sw__
  pack_brick_pre_sw_forward_x(mpp->send_prev, grid, archdata->pack_params + PACK_FWD_NEG_Z);
  pack_brick_pre_sw_forward_x(mpp->send_next, grid, archdata->pack_params + PACK_FWD_POS_Z);
  #else
  nsend_prev = pack_brick_forward_x(mpp->send_prev, grid, 0, nn, lo->y, hi->y, lo->z, hi->z);
  nsend_next = pack_brick_forward_x(mpp->send_next, grid, nlocal->x - nn, nlocal->x, lo->y, hi->y, lo->z, hi->z);
  #endif
  timer_stop(PACKX);
  COMM_ISEND(mpp, nsend_prev, prev, x);
  COMM_ISEND(mpp, nsend_next, next, x);
  COMM_WAITALL(mpp);
  timer_start(UNPACKX);
  #ifdef __sw__
  unpack_brick_pre_sw_forward_x(mpp->recv_prev, grid, archdata->pack_params + UNPACK_FWD_NEG_Z);
  unpack_brick_pre_sw_forward_x(mpp->recv_next, grid, archdata->pack_params + UNPACK_FWD_POS_Z);
  #else
  unpack_brick_forward_x(mpp->recv_prev, grid, lo->x, lo->x + nn, lo->y, hi->y, lo->z, hi->z);
  unpack_brick_forward_x(mpp->recv_next, grid, hi->x - nn, hi->x, lo->y, hi->y, lo->z, hi->z);
  #endif
  timer_stop(UNPACKX);
}

size_t pack_cell_reverse_f(void *buf, celldata_t *cell){
  void *ptr = buf;
  pack_field(ptr, cell->f, cell->natom);
  return ptr - buf;
}
//['f']
size_t unpack_cell_reverse_f(void *buf, celldata_t *cell){
  void *ptr = buf;
  vec<real> *f_buf = ptr;
  for (int i = 0; i < cell->natom; i ++){
    vecaddv(cell->f[i], cell->f[i], f_buf[i]);
  }
  ptr += sizeof(*(cell->f)) * cell->natom;
  return ptr - buf;
}

size_t pack_brick_reverse_f(void *buf, cellgrid_t *grid, int xlo, int xhi, int ylo, int yhi, int zlo, int zhi) {
#ifdef __sw__
  return pack_brick_sw_reverse_f(buf, grid, xlo, xhi, ylo, yhi, zlo, zhi);
#endif
  void *ptr = buf;
  for (int i = xlo; i < xhi; i ++) {
    for (int j = ylo; j < yhi; j ++) {
      for (int k = zlo; k < zhi; k ++) {
        celldata_t *cell = get_cell_xyz(grid, i, j, k);
        ptr += pack_cell_reverse_f(ptr, cell);
      }
    }
  }
  return ptr - buf;
}

size_t unpack_brick_reverse_f(void *buf, cellgrid_t *grid, int xlo, int xhi, int ylo, int yhi, int zlo, int zhi) {
#ifdef __sw__
  return unpack_brick_sw_reverse_f(buf, grid, xlo, xhi, ylo, yhi, zlo, zhi);
#endif
  void *ptr = buf;
  for (int i = xlo; i < xhi; i ++) {
    for (int j = ylo; j < yhi; j ++) {
      for (int k = zlo; k < zhi; k ++) {
        celldata_t *cell = get_cell_xyz(grid, i, j, k);
        ptr += unpack_cell_reverse_f(ptr, cell);
      }
    }
  }
  return ptr - buf;
}

void reverse_comm_f(cellgrid_t *grid, mpp_t *mpp) {
  vec<int> *lo = &(grid->dim.lo);
  vec<int> *hi = &(grid->dim.hi);
  vec<int> *nlocal = &(grid->nlocal);
  int nn = grid->nn;

  size_t nsend_prev, nsend_next;

  COMM_IRECV(mpp, prev, x);
  COMM_IRECV(mpp, next, x);
  nsend_prev = pack_brick_reverse_f(mpp->send_prev, grid, lo->x, lo->x + nn, lo->y, hi->y, lo->z, hi->z);
  nsend_next = pack_brick_reverse_f(mpp->send_next, grid, hi->x - nn, hi->x, lo->y, hi->y, lo->z, hi->z);
  COMM_ISEND(mpp, nsend_prev, prev, x);
  COMM_ISEND(mpp, nsend_next, next, x);
  COMM_WAITALL(mpp);
  unpack_brick_reverse_f(mpp->recv_prev, grid, 0, nn, lo->y, hi->y, lo->z, hi->z);
  unpack_brick_reverse_f(mpp->recv_next, grid, nlocal->x - nn, nlocal->x, lo->y, hi->y, lo->z, hi->z);

  COMM_IRECV(mpp, prev, y);
  COMM_IRECV(mpp, next, y);
  nsend_prev = pack_brick_reverse_f(mpp->send_prev, grid, 0, nlocal->x, lo->y, lo->y + nn, lo->z, hi->z);
  nsend_next = pack_brick_reverse_f(mpp->send_next, grid, 0, nlocal->x, hi->y - nn, hi->y, lo->z, hi->z);
  COMM_ISEND(mpp, nsend_prev, prev, y);
  COMM_ISEND(mpp, nsend_next, next, y);
  COMM_WAITALL(mpp);
  unpack_brick_reverse_f(mpp->recv_prev, grid, 0, nlocal->x, 0, nn, lo->z, hi->z);
  unpack_brick_reverse_f(mpp->recv_next, grid, 0, nlocal->x, nlocal->y - nn, nlocal->y, lo->z, hi->z);

  COMM_IRECV(mpp, prev, z);
  COMM_IRECV(mpp, next, z);
  nsend_prev = pack_brick_reverse_f(mpp->send_prev, grid, 0, nlocal->x, 0, nlocal->y, lo->z, lo->z + nn);
  nsend_next = pack_brick_reverse_f(mpp->send_next, grid, 0, nlocal->x, 0, nlocal->y, hi->z - nn, hi->z);
  COMM_ISEND(mpp, nsend_prev, prev, z);
  COMM_ISEND(mpp, nsend_next, next, z);
  COMM_WAITALL(mpp);
  unpack_brick_reverse_f(mpp->recv_prev, grid, 0, nlocal->x, 0, nlocal->y, 0, nn);
  unpack_brick_reverse_f(mpp->recv_next, grid, 0, nlocal->x, 0, nlocal->y, nlocal->z - nn, nlocal->z);
}

size_t pack_cell_forward_v(void *buf, celldata_t *cell){
  void *ptr = buf;
  pack_field(ptr, cell->v, cell->natom);
  return ptr - buf;
}
//['v']
size_t unpack_cell_forward_v(void *buf, celldata_t *cell){
  void *ptr = buf;
  unpack_field(ptr, cell->v, cell->natom);
  return ptr - buf;
}

size_t pack_brick_forward_v(void *buf, cellgrid_t *grid, int xlo, int xhi, int ylo, int yhi, int zlo, int zhi) {
  void *ptr = buf;
  for (int i = xlo; i < xhi; i ++) {
    for (int j = ylo; j < yhi; j ++) {
      for (int k = zlo; k < zhi; k ++) {
        celldata_t *cell = get_cell_xyz(grid, i, j, k);
        ptr += pack_cell_forward_v(ptr, cell);
      }
    }
  }
  return ptr - buf;
}

size_t unpack_brick_forward_v(void *buf, cellgrid_t *grid, int xlo, int xhi, int ylo, int yhi, int zlo, int zhi) {
  void *ptr = buf;
  for (int i = xlo; i < xhi; i ++) {
    for (int j = ylo; j < yhi; j ++) {
      for (int k = zlo; k < zhi; k ++) {
        celldata_t *cell = get_cell_xyz(grid, i, j, k);
        ptr += unpack_cell_forward_v(ptr, cell);
      }
    }
  }
  return ptr - buf;
}

void forward_comm_v(cellgrid_t *grid, mpp_t *mpp) {
  vec<int> *lo = &(grid->dim.lo);
  vec<int> *hi = &(grid->dim.hi);
  vec<int> *nlocal = &(grid->nlocal);
  int nn = grid->nn;

  size_t nsend_prev, nsend_next;

  COMM_IRECV(mpp, prev, z);
  COMM_IRECV(mpp, next, z);
  nsend_prev = pack_brick_forward_v(mpp->send_prev, grid, 0, nlocal->x, 0, nlocal->y, 0, nn);
  nsend_next = pack_brick_forward_v(mpp->send_next, grid, 0, nlocal->x, 0, nlocal->y, nlocal->z - nn, nlocal->z);
  COMM_ISEND(mpp, nsend_prev, prev, z);
  COMM_ISEND(mpp, nsend_next, next, z);
  COMM_WAITALL(mpp);
  unpack_brick_forward_v(mpp->recv_prev, grid, 0, nlocal->x, 0, nlocal->y, lo->z, lo->z + nn);
  unpack_brick_forward_v(mpp->recv_next, grid, 0, nlocal->x, 0, nlocal->y, hi->z - nn, hi->z);

  COMM_IRECV(mpp, prev, y);
  COMM_IRECV(mpp, next, y);
  nsend_prev = pack_brick_forward_v(mpp->send_prev, grid, 0, nlocal->x, 0, nn, lo->z, hi->z);
  nsend_next = pack_brick_forward_v(mpp->send_next, grid, 0, nlocal->x, nlocal->y - nn, nlocal->y, lo->z, hi->z);
  COMM_ISEND(mpp, nsend_prev, prev, y);
  COMM_ISEND(mpp, nsend_next, next, y);
  COMM_WAITALL(mpp);
  unpack_brick_forward_v(mpp->recv_prev, grid, 0, nlocal->x, lo->y, lo->y + nn, lo->z, hi->z);
  unpack_brick_forward_v(mpp->recv_next, grid, 0, nlocal->x, hi->y - nn, hi->y, lo->z, hi->z);

  COMM_IRECV(mpp, prev, x);
  COMM_IRECV(mpp, next, x);
  nsend_prev = pack_brick_forward_v(mpp->send_prev, grid, 0, nn, lo->y, hi->y, lo->z, hi->z);
  nsend_next = pack_brick_forward_v(mpp->send_next, grid, nlocal->x - nn, nlocal->x, lo->y, hi->y, lo->z, hi->z);
  COMM_ISEND(mpp, nsend_prev, prev, x);
  COMM_ISEND(mpp, nsend_next, next, x);
  COMM_WAITALL(mpp);
  unpack_brick_forward_v(mpp->recv_prev, grid, lo->x, lo->x + nn, lo->y, hi->y, lo->z, hi->z);
  unpack_brick_forward_v(mpp->recv_next, grid, hi->x - nn, hi->x, lo->y, hi->y, lo->z, hi->z);
}

size_t pack_cell_reverse_v(void *buf, celldata_t *cell){
  void *ptr = buf;
  pack_field(ptr, cell->v, cell->natom);
  return ptr - buf;
}
//['v']
size_t unpack_cell_reverse_v(void *buf, celldata_t *cell){
  void *ptr = buf;
  vec<real> *v_buf = ptr;
  for (int i = 0; i < cell->natom; i ++){
    vecaddv(cell->v[i], cell->v[i], v_buf[i]);
  }
  ptr += sizeof(*(cell->v)) * cell->natom;
  return ptr - buf;
}

size_t pack_brick_reverse_v(void *buf, cellgrid_t *grid, int xlo, int xhi, int ylo, int yhi, int zlo, int zhi) {
  void *ptr = buf;
  for (int i = xlo; i < xhi; i ++) {
    for (int j = ylo; j < yhi; j ++) {
      for (int k = zlo; k < zhi; k ++) {
        celldata_t *cell = get_cell_xyz(grid, i, j, k);
        ptr += pack_cell_reverse_v(ptr, cell);
      }
    }
  }
  return ptr - buf;
}

size_t unpack_brick_reverse_v(void *buf, cellgrid_t *grid, int xlo, int xhi, int ylo, int yhi, int zlo, int zhi) {
  void *ptr = buf;
  for (int i = xlo; i < xhi; i ++) {
    for (int j = ylo; j < yhi; j ++) {
      for (int k = zlo; k < zhi; k ++) {
        celldata_t *cell = get_cell_xyz(grid, i, j, k);
        ptr += unpack_cell_reverse_v(ptr, cell);
      }
    }
  }
  return ptr - buf;
}

void reverse_comm_v(cellgrid_t *grid, mpp_t *mpp) {
  vec<int> *lo = &(grid->dim.lo);
  vec<int> *hi = &(grid->dim.hi);
  vec<int> *nlocal = &(grid->nlocal);
  int nn = grid->nn;

  size_t nsend_prev, nsend_next;

  COMM_IRECV(mpp, prev, x);
  COMM_IRECV(mpp, next, x);
  nsend_prev = pack_brick_reverse_v(mpp->send_prev, grid, lo->x, lo->x + nn, lo->y, hi->y, lo->z, hi->z);
  nsend_next = pack_brick_reverse_v(mpp->send_next, grid, hi->x - nn, hi->x, lo->y, hi->y, lo->z, hi->z);
  COMM_ISEND(mpp, nsend_prev, prev, x);
  COMM_ISEND(mpp, nsend_next, next, x);
  COMM_WAITALL(mpp);
  unpack_brick_reverse_v(mpp->recv_prev, grid, 0, nn, lo->y, hi->y, lo->z, hi->z);
  unpack_brick_reverse_v(mpp->recv_next, grid, nlocal->x - nn, nlocal->x, lo->y, hi->y, lo->z, hi->z);

  COMM_IRECV(mpp, prev, y);
  COMM_IRECV(mpp, next, y);
  nsend_prev = pack_brick_reverse_v(mpp->send_prev, grid, 0, nlocal->x, lo->y, lo->y + nn, lo->z, hi->z);
  nsend_next = pack_brick_reverse_v(mpp->send_next, grid, 0, nlocal->x, hi->y - nn, hi->y, lo->z, hi->z);
  COMM_ISEND(mpp, nsend_prev, prev, y);
  COMM_ISEND(mpp, nsend_next, next, y);
  COMM_WAITALL(mpp);
  unpack_brick_reverse_v(mpp->recv_prev, grid, 0, nlocal->x, 0, nn, lo->z, hi->z);
  unpack_brick_reverse_v(mpp->recv_next, grid, 0, nlocal->x, nlocal->y - nn, nlocal->y, lo->z, hi->z);

  COMM_IRECV(mpp, prev, z);
  COMM_IRECV(mpp, next, z);
  nsend_prev = pack_brick_reverse_v(mpp->send_prev, grid, 0, nlocal->x, 0, nlocal->y, lo->z, lo->z + nn);
  nsend_next = pack_brick_reverse_v(mpp->send_next, grid, 0, nlocal->x, 0, nlocal->y, hi->z - nn, hi->z);
  COMM_ISEND(mpp, nsend_prev, prev, z);
  COMM_ISEND(mpp, nsend_next, next, z);
  COMM_WAITALL(mpp);
  unpack_brick_reverse_v(mpp->recv_prev, grid, 0, nlocal->x, 0, nlocal->y, 0, nn);
  unpack_brick_reverse_v(mpp->recv_next, grid, 0, nlocal->x, 0, nlocal->y, nlocal->z - nn, nlocal->z);
}

size_t pack_cell_forward_export_list(void *buf, celldata_t *cell){
  *(long*)buf = cell->nexport;
  void *ptr = buf + 8;
  pack_field(ptr, cell->x + (CELL_CAP - cell->nexport), cell->nexport);
  pack_field(ptr, cell->q + (CELL_CAP - cell->nexport), cell->nexport);
  pack_field(ptr, cell->tag + (CELL_CAP - cell->nexport), cell->nexport);
  pack_field(ptr, cell->t + (CELL_CAP - cell->nexport), cell->nexport);
  pack_field(ptr, cell->v + (CELL_CAP - cell->nexport), cell->nexport);
  pack_field(ptr, cell->mass + (CELL_CAP - cell->nexport), cell->nexport);
  *(long*)ptr = cell->nbonded_export;
  ptr += 8;
  *(long*)ptr = cell->nchain2_export;
  ptr += 8;
  *(long*)ptr = cell->nscal_export;
  ptr += 8;
  *(long*)ptr = cell->nexcl_export;
  ptr += 8;
  *(long*)ptr = cell->nimpr_export;
  ptr += 8;
  pack_field(ptr, cell->first_bonded + (CELL_CAP - cell->nexport), cell->nexport + 1);
  pack_field(ptr, cell->first_chain2 + (CELL_CAP - cell->nexport), cell->nexport + 1);
  pack_field(ptr, cell->first_scal_atom + (CELL_CAP - cell->nexport), cell->nexport + 1);
  pack_field(ptr, cell->first_excl_atom + (CELL_CAP - cell->nexport), cell->nexport + 1);
  pack_field(ptr, cell->first_impr + (CELL_CAP - cell->nexport), cell->nexport + 1);
  pack_field(ptr, cell->bonded_tag + (MAX_BONDED_CELL - cell->nbonded_export), cell->nbonded_export);
  pack_field(ptr, cell->chain2_tag + (MAX_CHAIN2_CELL - cell->nchain2_export), cell->nchain2_export);
  pack_field(ptr, cell->excl_tag + (MAX_EXCL_CELL - cell->nexcl_export), cell->nexcl_export);
  pack_field(ptr, cell->scal_tag + (MAX_SCAL_CELL - cell->nscal_export), cell->nscal_export);
  pack_field(ptr, cell->impr_idx + (MAX_IMPR_CELL - cell->nimpr_export), cell->nimpr_export);
  pack_field(ptr, cell->shake + (CELL_CAP - cell->nexport), cell->nexport);
  return ptr - buf;
}
//['x', 'q', 'tag', 't', 'v', 'mass', 'nbonded_export', 'nchain2_export', 'nscal_export', 'nexcl_export', 'nimpr_export', 'first_bonded', 'first_chain2', 'first_scal_atom', 'first_excl_atom', 'first_impr', 'bonded_tag', 'chain2_tag', 'excl_tag', 'scal_tag', 'impr_idx', 'shake']
size_t unpack_cell_forward_export_list(void *buf, celldata_t *cell){
  cell->nexport = *(long*)buf;
  void *ptr = buf + 8;
  unpack_field(ptr, cell->x + (CELL_CAP - cell->nexport), cell->nexport);
  unpack_field(ptr, cell->q + (CELL_CAP - cell->nexport), cell->nexport);
  unpack_field(ptr, cell->tag + (CELL_CAP - cell->nexport), cell->nexport);
  unpack_field(ptr, cell->t + (CELL_CAP - cell->nexport), cell->nexport);
  unpack_field(ptr, cell->v + (CELL_CAP - cell->nexport), cell->nexport);
  unpack_field(ptr, cell->mass + (CELL_CAP - cell->nexport), cell->nexport);
  cell->nbonded_export = *(long*)ptr;
  ptr += 8;
  cell->nchain2_export = *(long*)ptr;
  ptr += 8;
  cell->nscal_export = *(long*)ptr;
  ptr += 8;
  cell->nexcl_export = *(long*)ptr;
  ptr += 8;
  cell->nimpr_export = *(long*)ptr;
  ptr += 8;
  unpack_field(ptr, cell->first_bonded + (CELL_CAP - cell->nexport), cell->nexport + 1);
  unpack_field(ptr, cell->first_chain2 + (CELL_CAP - cell->nexport), cell->nexport + 1);
  unpack_field(ptr, cell->first_scal_atom + (CELL_CAP - cell->nexport), cell->nexport + 1);
  unpack_field(ptr, cell->first_excl_atom + (CELL_CAP - cell->nexport), cell->nexport + 1);
  unpack_field(ptr, cell->first_impr + (CELL_CAP - cell->nexport), cell->nexport + 1);
  unpack_field(ptr, cell->bonded_tag + (MAX_BONDED_CELL - cell->nbonded_export), cell->nbonded_export);
  unpack_field(ptr, cell->chain2_tag + (MAX_CHAIN2_CELL - cell->nchain2_export), cell->nchain2_export);
  unpack_field(ptr, cell->excl_tag + (MAX_EXCL_CELL - cell->nexcl_export), cell->nexcl_export);
  unpack_field(ptr, cell->scal_tag + (MAX_SCAL_CELL - cell->nscal_export), cell->nscal_export);
  unpack_field(ptr, cell->impr_idx + (MAX_IMPR_CELL - cell->nimpr_export), cell->nimpr_export);
  unpack_field(ptr, cell->shake + (CELL_CAP - cell->nexport), cell->nexport);
  return ptr - buf;
}

size_t pack_brick_forward_export_list(void *buf, cellgrid_t *grid, int xlo, int xhi, int ylo, int yhi, int zlo, int zhi) {
  void *ptr = buf;
  for (int i = xlo; i < xhi; i ++) {
    for (int j = ylo; j < yhi; j ++) {
      for (int k = zlo; k < zhi; k ++) {
        celldata_t *cell = get_cell_xyz(grid, i, j, k);
        ptr += pack_cell_forward_export_list(ptr, cell);
      }
    }
  }
  return ptr - buf;
}

size_t unpack_brick_forward_export_list(void *buf, cellgrid_t *grid, int xlo, int xhi, int ylo, int yhi, int zlo, int zhi) {
  void *ptr = buf;
  for (int i = xlo; i < xhi; i ++) {
    for (int j = ylo; j < yhi; j ++) {
      for (int k = zlo; k < zhi; k ++) {
        celldata_t *cell = get_cell_xyz(grid, i, j, k);
        ptr += unpack_cell_forward_export_list(ptr, cell);
      }
    }
  }
  return ptr - buf;
}

void forward_comm_export_list(cellgrid_t *grid, mpp_t *mpp) {
  vec<int> *lo = &(grid->dim.lo);
  vec<int> *hi = &(grid->dim.hi);
  vec<int> *nlocal = &(grid->nlocal);
  int nn = grid->nn;

  size_t nsend_prev, nsend_next;

  COMM_IRECV(mpp, prev, z);
  COMM_IRECV(mpp, next, z);
  nsend_prev = pack_brick_forward_export_list(mpp->send_prev, grid, 0, nlocal->x, 0, nlocal->y, 0, nn);
  nsend_next = pack_brick_forward_export_list(mpp->send_next, grid, 0, nlocal->x, 0, nlocal->y, nlocal->z - nn, nlocal->z);
  COMM_ISEND(mpp, nsend_prev, prev, z);
  COMM_ISEND(mpp, nsend_next, next, z);
  COMM_WAITALL(mpp);
  unpack_brick_forward_export_list(mpp->recv_prev, grid, 0, nlocal->x, 0, nlocal->y, lo->z, lo->z + nn);
  unpack_brick_forward_export_list(mpp->recv_next, grid, 0, nlocal->x, 0, nlocal->y, hi->z - nn, hi->z);

  COMM_IRECV(mpp, prev, y);
  COMM_IRECV(mpp, next, y);
  nsend_prev = pack_brick_forward_export_list(mpp->send_prev, grid, 0, nlocal->x, 0, nn, lo->z, hi->z);
  nsend_next = pack_brick_forward_export_list(mpp->send_next, grid, 0, nlocal->x, nlocal->y - nn, nlocal->y, lo->z, hi->z);
  COMM_ISEND(mpp, nsend_prev, prev, y);
  COMM_ISEND(mpp, nsend_next, next, y);
  COMM_WAITALL(mpp);
  unpack_brick_forward_export_list(mpp->recv_prev, grid, 0, nlocal->x, lo->y, lo->y + nn, lo->z, hi->z);
  unpack_brick_forward_export_list(mpp->recv_next, grid, 0, nlocal->x, hi->y - nn, hi->y, lo->z, hi->z);

  COMM_IRECV(mpp, prev, x);
  COMM_IRECV(mpp, next, x);
  nsend_prev = pack_brick_forward_export_list(mpp->send_prev, grid, 0, nn, lo->y, hi->y, lo->z, hi->z);
  nsend_next = pack_brick_forward_export_list(mpp->send_next, grid, nlocal->x - nn, nlocal->x, lo->y, hi->y, lo->z, hi->z);
  COMM_ISEND(mpp, nsend_prev, prev, x);
  COMM_ISEND(mpp, nsend_next, next, x);
  COMM_WAITALL(mpp);
  unpack_brick_forward_export_list(mpp->recv_prev, grid, lo->x, lo->x + nn, lo->y, hi->y, lo->z, hi->z);
  unpack_brick_forward_export_list(mpp->recv_next, grid, hi->x - nn, hi->x, lo->y, hi->y, lo->z, hi->z);
}

size_t pack_cell_forward_export_list_cg(void *buf, celldata_t *cell){
  *(long*)buf = cell->nexport;
  void *ptr = buf + 8;
  pack_field(ptr, cell->x + (CELL_CAP - cell->nexport), cell->nexport);
  pack_field(ptr, cell->q + (CELL_CAP - cell->nexport), cell->nexport);
  pack_field(ptr, cell->tag + (CELL_CAP - cell->nexport), cell->nexport);
  pack_field(ptr, cell->t + (CELL_CAP - cell->nexport), cell->nexport);
  pack_field(ptr, cell->v + (CELL_CAP - cell->nexport), cell->nexport);
  pack_field(ptr, cell->mass + (CELL_CAP - cell->nexport), cell->nexport);
  *(long*)ptr = cell->nbonded_export;
  ptr += 8;
  *(long*)ptr = cell->nchain2_export;
  ptr += 8;
  *(long*)ptr = cell->nscal_export;
  ptr += 8;
  *(long*)ptr = cell->nexcl_export;
  ptr += 8;
  *(long*)ptr = cell->nimpr_export;
  ptr += 8;
  pack_field(ptr, cell->first_bonded + (CELL_CAP - cell->nexport), cell->nexport + 1);
  pack_field(ptr, cell->first_chain2 + (CELL_CAP - cell->nexport), cell->nexport + 1);
  pack_field(ptr, cell->first_scal_atom + (CELL_CAP - cell->nexport), cell->nexport + 1);
  pack_field(ptr, cell->first_excl_atom + (CELL_CAP - cell->nexport), cell->nexport + 1);
  pack_field(ptr, cell->first_impr + (CELL_CAP - cell->nexport), cell->nexport + 1);
  pack_field(ptr, cell->bonded_tag + (MAX_BONDED_CELL - cell->nbonded_export), cell->nbonded_export);
  pack_field(ptr, cell->chain2_tag + (MAX_CHAIN2_CELL - cell->nchain2_export), cell->nchain2_export);
  pack_field(ptr, cell->excl_tag + (MAX_EXCL_CELL - cell->nexcl_export), cell->nexcl_export);
  pack_field(ptr, cell->scal_tag + (MAX_SCAL_CELL - cell->nscal_export), cell->nscal_export);
  pack_field(ptr, cell->impr_idx + (MAX_IMPR_CELL - cell->nimpr_export), cell->nimpr_export);
  pack_field(ptr, cell->shake + (CELL_CAP - cell->nexport), cell->nexport);
  pack_field(ptr, cell->shake_tmp + (CELL_CAP - cell->nexport), cell->nexport);
  return ptr - buf;
}
//['x', 'q', 'tag', 't', 'v', 'mass', 'nbonded_export', 'nchain2_export', 'nscal_export', 'nexcl_export', 'nimpr_export', 'first_bonded', 'first_chain2', 'first_scal_atom', 'first_excl_atom', 'first_impr', 'bonded_tag', 'chain2_tag', 'excl_tag', 'scal_tag', 'impr_idx', 'shake', 'shake_tmp']
size_t unpack_cell_forward_export_list_cg(void *buf, celldata_t *cell){
  cell->nexport = *(long*)buf;
  void *ptr = buf + 8;
  unpack_field(ptr, cell->x + (CELL_CAP - cell->nexport), cell->nexport);
  unpack_field(ptr, cell->q + (CELL_CAP - cell->nexport), cell->nexport);
  unpack_field(ptr, cell->tag + (CELL_CAP - cell->nexport), cell->nexport);
  unpack_field(ptr, cell->t + (CELL_CAP - cell->nexport), cell->nexport);
  unpack_field(ptr, cell->v + (CELL_CAP - cell->nexport), cell->nexport);
  unpack_field(ptr, cell->mass + (CELL_CAP - cell->nexport), cell->nexport);
  cell->nbonded_export = *(long*)ptr;
  ptr += 8;
  cell->nchain2_export = *(long*)ptr;
  ptr += 8;
  cell->nscal_export = *(long*)ptr;
  ptr += 8;
  cell->nexcl_export = *(long*)ptr;
  ptr += 8;
  cell->nimpr_export = *(long*)ptr;
  ptr += 8;
  unpack_field(ptr, cell->first_bonded + (CELL_CAP - cell->nexport), cell->nexport + 1);
  unpack_field(ptr, cell->first_chain2 + (CELL_CAP - cell->nexport), cell->nexport + 1);
  unpack_field(ptr, cell->first_scal_atom + (CELL_CAP - cell->nexport), cell->nexport + 1);
  unpack_field(ptr, cell->first_excl_atom + (CELL_CAP - cell->nexport), cell->nexport + 1);
  unpack_field(ptr, cell->first_impr + (CELL_CAP - cell->nexport), cell->nexport + 1);
  unpack_field(ptr, cell->bonded_tag + (MAX_BONDED_CELL - cell->nbonded_export), cell->nbonded_export);
  unpack_field(ptr, cell->chain2_tag + (MAX_CHAIN2_CELL - cell->nchain2_export), cell->nchain2_export);
  unpack_field(ptr, cell->excl_tag + (MAX_EXCL_CELL - cell->nexcl_export), cell->nexcl_export);
  unpack_field(ptr, cell->scal_tag + (MAX_SCAL_CELL - cell->nscal_export), cell->nscal_export);
  unpack_field(ptr, cell->impr_idx + (MAX_IMPR_CELL - cell->nimpr_export), cell->nimpr_export);
  unpack_field(ptr, cell->shake + (CELL_CAP - cell->nexport), cell->nexport);
  unpack_field(ptr, cell->shake_tmp + (CELL_CAP - cell->nexport), cell->nexport);
  return ptr - buf;
}

size_t pack_brick_forward_export_list_cg(void *buf, cellgrid_t *grid, int xlo, int xhi, int ylo, int yhi, int zlo, int zhi) {
  void *ptr = buf;
  for (int i = xlo; i < xhi; i ++) {
    for (int j = ylo; j < yhi; j ++) {
      for (int k = zlo; k < zhi; k ++) {
        celldata_t *cell = get_cell_xyz(grid, i, j, k);
        ptr += pack_cell_forward_export_list_cg(ptr, cell);
      }
    }
  }
  return ptr - buf;
}

size_t unpack_brick_forward_export_list_cg(void *buf, cellgrid_t *grid, int xlo, int xhi, int ylo, int yhi, int zlo, int zhi) {
  void *ptr = buf;
  for (int i = xlo; i < xhi; i ++) {
    for (int j = ylo; j < yhi; j ++) {
      for (int k = zlo; k < zhi; k ++) {
        celldata_t *cell = get_cell_xyz(grid, i, j, k);
        ptr += unpack_cell_forward_export_list_cg(ptr, cell);
      }
    }
  }
  return ptr - buf;
}

void forward_comm_export_list_cg(cellgrid_t *grid, mpp_t *mpp) {
  vec<int> *lo = &(grid->dim.lo);
  vec<int> *hi = &(grid->dim.hi);
  vec<int> *nlocal = &(grid->nlocal);
  int nn = grid->nn;

  size_t nsend_prev, nsend_next;

  COMM_IRECV(mpp, prev, z);
  COMM_IRECV(mpp, next, z);
  nsend_prev = pack_brick_forward_export_list_cg(mpp->send_prev, grid, 0, nlocal->x, 0, nlocal->y, 0, nn);
  nsend_next = pack_brick_forward_export_list_cg(mpp->send_next, grid, 0, nlocal->x, 0, nlocal->y, nlocal->z - nn, nlocal->z);
  COMM_ISEND(mpp, nsend_prev, prev, z);
  COMM_ISEND(mpp, nsend_next, next, z);
  COMM_WAITALL(mpp);
  unpack_brick_forward_export_list_cg(mpp->recv_prev, grid, 0, nlocal->x, 0, nlocal->y, lo->z, lo->z + nn);
  unpack_brick_forward_export_list_cg(mpp->recv_next, grid, 0, nlocal->x, 0, nlocal->y, hi->z - nn, hi->z);

  COMM_IRECV(mpp, prev, y);
  COMM_IRECV(mpp, next, y);
  nsend_prev = pack_brick_forward_export_list_cg(mpp->send_prev, grid, 0, nlocal->x, 0, nn, lo->z, hi->z);
  nsend_next = pack_brick_forward_export_list_cg(mpp->send_next, grid, 0, nlocal->x, nlocal->y - nn, nlocal->y, lo->z, hi->z);
  COMM_ISEND(mpp, nsend_prev, prev, y);
  COMM_ISEND(mpp, nsend_next, next, y);
  COMM_WAITALL(mpp);
  unpack_brick_forward_export_list_cg(mpp->recv_prev, grid, 0, nlocal->x, lo->y, lo->y + nn, lo->z, hi->z);
  unpack_brick_forward_export_list_cg(mpp->recv_next, grid, 0, nlocal->x, hi->y - nn, hi->y, lo->z, hi->z);

  COMM_IRECV(mpp, prev, x);
  COMM_IRECV(mpp, next, x);
  nsend_prev = pack_brick_forward_export_list_cg(mpp->send_prev, grid, 0, nn, lo->y, hi->y, lo->z, hi->z);
  nsend_next = pack_brick_forward_export_list_cg(mpp->send_next, grid, nlocal->x - nn, nlocal->x, lo->y, hi->y, lo->z, hi->z);
  COMM_ISEND(mpp, nsend_prev, prev, x);
  COMM_ISEND(mpp, nsend_next, next, x);
  COMM_WAITALL(mpp);
  unpack_brick_forward_export_list_cg(mpp->recv_prev, grid, lo->x, lo->x + nn, lo->y, hi->y, lo->z, hi->z);
  unpack_brick_forward_export_list_cg(mpp->recv_next, grid, hi->x - nn, hi->x, lo->y, hi->y, lo->z, hi->z);
}

size_t pack_cell_forward_shake(void *buf, celldata_t *cell){
  void *ptr = buf;
  pack_field(ptr, cell->shake_tmp, cell->natom);
  return ptr - buf;
}
//['shake_tmp']
size_t unpack_cell_forward_shake(void *buf, celldata_t *cell){
  void *ptr = buf;
  unpack_field(ptr, cell->shake_tmp, cell->natom);
  return ptr - buf;
}

size_t pack_brick_forward_shake(void *buf, cellgrid_t *grid, int xlo, int xhi, int ylo, int yhi, int zlo, int zhi) {
  void *ptr = buf;
  for (int i = xlo; i < xhi; i ++) {
    for (int j = ylo; j < yhi; j ++) {
      for (int k = zlo; k < zhi; k ++) {
        celldata_t *cell = get_cell_xyz(grid, i, j, k);
        ptr += pack_cell_forward_shake(ptr, cell);
      }
    }
  }
  return ptr - buf;
}

size_t unpack_brick_forward_shake(void *buf, cellgrid_t *grid, int xlo, int xhi, int ylo, int yhi, int zlo, int zhi) {
  void *ptr = buf;
  for (int i = xlo; i < xhi; i ++) {
    for (int j = ylo; j < yhi; j ++) {
      for (int k = zlo; k < zhi; k ++) {
        celldata_t *cell = get_cell_xyz(grid, i, j, k);
        ptr += unpack_cell_forward_shake(ptr, cell);
      }
    }
  }
  return ptr - buf;
}

void forward_comm_shake(cellgrid_t *grid, mpp_t *mpp) {
  vec<int> *lo = &(grid->dim.lo);
  vec<int> *hi = &(grid->dim.hi);
  vec<int> *nlocal = &(grid->nlocal);
  int nn = grid->nn;

  size_t nsend_prev, nsend_next;

  COMM_IRECV(mpp, prev, z);
  COMM_IRECV(mpp, next, z);
  nsend_prev = pack_brick_forward_shake(mpp->send_prev, grid, 0, nlocal->x, 0, nlocal->y, 0, nn);
  nsend_next = pack_brick_forward_shake(mpp->send_next, grid, 0, nlocal->x, 0, nlocal->y, nlocal->z - nn, nlocal->z);
  COMM_ISEND(mpp, nsend_prev, prev, z);
  COMM_ISEND(mpp, nsend_next, next, z);
  COMM_WAITALL(mpp);
  unpack_brick_forward_shake(mpp->recv_prev, grid, 0, nlocal->x, 0, nlocal->y, lo->z, lo->z + nn);
  unpack_brick_forward_shake(mpp->recv_next, grid, 0, nlocal->x, 0, nlocal->y, hi->z - nn, hi->z);

  COMM_IRECV(mpp, prev, y);
  COMM_IRECV(mpp, next, y);
  nsend_prev = pack_brick_forward_shake(mpp->send_prev, grid, 0, nlocal->x, 0, nn, lo->z, hi->z);
  nsend_next = pack_brick_forward_shake(mpp->send_next, grid, 0, nlocal->x, nlocal->y - nn, nlocal->y, lo->z, hi->z);
  COMM_ISEND(mpp, nsend_prev, prev, y);
  COMM_ISEND(mpp, nsend_next, next, y);
  COMM_WAITALL(mpp);
  unpack_brick_forward_shake(mpp->recv_prev, grid, 0, nlocal->x, lo->y, lo->y + nn, lo->z, hi->z);
  unpack_brick_forward_shake(mpp->recv_next, grid, 0, nlocal->x, hi->y - nn, hi->y, lo->z, hi->z);

  COMM_IRECV(mpp, prev, x);
  COMM_IRECV(mpp, next, x);
  nsend_prev = pack_brick_forward_shake(mpp->send_prev, grid, 0, nn, lo->y, hi->y, lo->z, hi->z);
  nsend_next = pack_brick_forward_shake(mpp->send_next, grid, nlocal->x - nn, nlocal->x, lo->y, hi->y, lo->z, hi->z);
  COMM_ISEND(mpp, nsend_prev, prev, x);
  COMM_ISEND(mpp, nsend_next, next, x);
  COMM_WAITALL(mpp);
  unpack_brick_forward_shake(mpp->recv_prev, grid, lo->x, lo->x + nn, lo->y, hi->y, lo->z, hi->z);
  unpack_brick_forward_shake(mpp->recv_next, grid, hi->x - nn, hi->x, lo->y, hi->y, lo->z, hi->z);
}

size_t pack_cell_reverse_shake(void *buf, celldata_t *cell){
  void *ptr = buf;
  pack_field(ptr, cell->shake_tmp, cell->natom);
  return ptr - buf;
}
//['shake_tmp']
size_t unpack_cell_reverse_shake(void *buf, celldata_t *cell){
  void *ptr = buf;
  vec<real> *shake_tmp_buf = ptr;
  for (int i = 0; i < cell->natom; i ++){
    vecaddv(cell->shake_tmp[i], cell->shake_tmp[i], shake_tmp_buf[i]);
  }
  ptr += sizeof(*(cell->shake_tmp)) * cell->natom;
  return ptr - buf;
}

size_t pack_brick_reverse_shake(void *buf, cellgrid_t *grid, int xlo, int xhi, int ylo, int yhi, int zlo, int zhi) {
  void *ptr = buf;
  for (int i = xlo; i < xhi; i ++) {
    for (int j = ylo; j < yhi; j ++) {
      for (int k = zlo; k < zhi; k ++) {
        celldata_t *cell = get_cell_xyz(grid, i, j, k);
        ptr += pack_cell_reverse_shake(ptr, cell);
      }
    }
  }
  return ptr - buf;
}

size_t unpack_brick_reverse_shake(void *buf, cellgrid_t *grid, int xlo, int xhi, int ylo, int yhi, int zlo, int zhi) {
  void *ptr = buf;
  for (int i = xlo; i < xhi; i ++) {
    for (int j = ylo; j < yhi; j ++) {
      for (int k = zlo; k < zhi; k ++) {
        celldata_t *cell = get_cell_xyz(grid, i, j, k);
        ptr += unpack_cell_reverse_shake(ptr, cell);
      }
    }
  }
  return ptr - buf;
}

void reverse_comm_shake(cellgrid_t *grid, mpp_t *mpp) {
  vec<int> *lo = &(grid->dim.lo);
  vec<int> *hi = &(grid->dim.hi);
  vec<int> *nlocal = &(grid->nlocal);
  int nn = grid->nn;

  size_t nsend_prev, nsend_next;

  COMM_IRECV(mpp, prev, x);
  COMM_IRECV(mpp, next, x);
  nsend_prev = pack_brick_reverse_shake(mpp->send_prev, grid, lo->x, lo->x + nn, lo->y, hi->y, lo->z, hi->z);
  nsend_next = pack_brick_reverse_shake(mpp->send_next, grid, hi->x - nn, hi->x, lo->y, hi->y, lo->z, hi->z);
  COMM_ISEND(mpp, nsend_prev, prev, x);
  COMM_ISEND(mpp, nsend_next, next, x);
  COMM_WAITALL(mpp);
  unpack_brick_reverse_shake(mpp->recv_prev, grid, 0, nn, lo->y, hi->y, lo->z, hi->z);
  unpack_brick_reverse_shake(mpp->recv_next, grid, nlocal->x - nn, nlocal->x, lo->y, hi->y, lo->z, hi->z);

  COMM_IRECV(mpp, prev, y);
  COMM_IRECV(mpp, next, y);
  nsend_prev = pack_brick_reverse_shake(mpp->send_prev, grid, 0, nlocal->x, lo->y, lo->y + nn, lo->z, hi->z);
  nsend_next = pack_brick_reverse_shake(mpp->send_next, grid, 0, nlocal->x, hi->y - nn, hi->y, lo->z, hi->z);
  COMM_ISEND(mpp, nsend_prev, prev, y);
  COMM_ISEND(mpp, nsend_next, next, y);
  COMM_WAITALL(mpp);
  unpack_brick_reverse_shake(mpp->recv_prev, grid, 0, nlocal->x, 0, nn, lo->z, hi->z);
  unpack_brick_reverse_shake(mpp->recv_next, grid, 0, nlocal->x, nlocal->y - nn, nlocal->y, lo->z, hi->z);

  COMM_IRECV(mpp, prev, z);
  COMM_IRECV(mpp, next, z);
  nsend_prev = pack_brick_reverse_shake(mpp->send_prev, grid, 0, nlocal->x, 0, nlocal->y, lo->z, lo->z + nn);
  nsend_next = pack_brick_reverse_shake(mpp->send_next, grid, 0, nlocal->x, 0, nlocal->y, hi->z - nn, hi->z);
  COMM_ISEND(mpp, nsend_prev, prev, z);
  COMM_ISEND(mpp, nsend_next, next, z);
  COMM_WAITALL(mpp);
  unpack_brick_reverse_shake(mpp->recv_prev, grid, 0, nlocal->x, 0, nlocal->y, 0, nn);
  unpack_brick_reverse_shake(mpp->recv_next, grid, 0, nlocal->x, 0, nlocal->y, nlocal->z - nn, nlocal->z);
}

