#if defined(WIN32) || defined(_WIN32) 
#define PATH_SEP "\\" 
#else 
#define PATH_SEP "/" 
#endif 
#include <wordexp.h>
#include <cstdio>
#include <cstdlib>
#include <climits>
#include <cassert>
#include <cstring>
static void cat_expand_path(char *ret, const char *root, const char *file){
  char pathraw[PATH_MAX];
  sprintf(pathraw, "%s%s%s", root, PATH_SEP, file);
  wordexp_t p;
  wordexp(pathraw, &p, WRDE_NOCMD);
  assert(p.we_wordc == 1);
  strcpy(ret, p.we_wordv[0]);
  wordfree(&p);
}
template<typename ...Ts>
static void expand_path(char *ret, const char *root, const char *fmt, Ts ...args){
  char pathraw[PATH_MAX];
  char allfmt[PATH_MAX];
  allfmt[0] = 0;
  strcat(allfmt, "%s%s");
  strcat(allfmt, fmt);
  sprintf(pathraw, allfmt, root, PATH_SEP, args...);

  wordexp_t p;
  wordexp(pathraw, &p, WRDE_NOCMD);
  assert(p.we_wordc == 1);
  strcpy(ret, p.we_wordv[0]);
  wordfree(&p);
}
