#pragma once
union elemtag{
  unsigned long ival;
  char str[8];
  bool operator ==(const elemtag &o) const {
    return ival == o.ival;
  }
  bool operator <(const elemtag &o) const {
    return ival < o.ival;
  }
};
constexpr elemtag str2tag(const char *s){
  elemtag ret = {0};
  ret.ival = 0;
  for (int i = 0; s[i]; i ++) {
    ret.str[i] = s[i];
  }
  return ret;
}
