#if __INTELLISENSE__
#undef va_start(arg, va)
#define va_start(arg, va)
#undef va_end(va)
#define va_end(va)
#undef va_copy(va0, va1)
#define va_copy(arg0, va1)
#define __INT_MAX__ 0x7fffffff
#endif
