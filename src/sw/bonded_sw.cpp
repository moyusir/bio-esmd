#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <math.h>
#include <qthread.h>
#include "esmd_types.h"
#include "cell.h"
#include "bonded.h"
#include "swarch.h"
#ifdef __sw_slave__
#include "cal.h"
#endif
static int cmp_tag(const void *va, const void *vb) {
  const long a = *(long *)va;
  const long b = *(long *)vb;
  return (a > b) - (a < b);
}
static int unique_tag(long *base, int n) {
  int n_uniq = 1;
  for (int i = 1; i < n; i++) {
    if (base[i] != base[i - 1]) {
      base[n_uniq++] = base[i];
    }
  }
  return n_uniq;
}
INLINE int search_tag(celldata_t *cell, long tag) {
  for (int ig = 0; ig < cell->nguest; ig++) {
    if (tag == cell->tag[CELL_CAP + ig]) {
      return CELL_CAP + ig;
    }
  }
  for (int ig = 0; ig < cell->natom; ig++) {
    if (tag == cell->tag[ig]) {
      return ig;
    }
  }
  return 0x7fffffff;
}
INLINE int search_tag_arr(long *tags, int natom, int nguest, long tag) {
  for (int ig = 0; ig < nguest; ig++) {
    if (tag == tags[CELL_CAP + ig]) {
      return CELL_CAP + ig;
    }
  }
  for (int ig = 0; ig < natom; ig++) {
    if (tag == tags[ig]) {
      return ig;
    }
  }
  #ifdef __sw_slave__
  cal_locked_printf("cannot find %d\n", tag);
  #else
  printf("cannot find %d\n", tag);
  #endif
  assert(0);
}
INLINE void htag4(long *out, long in) {
  out[0] |= 1L << ((in >>  0) & 63L);
  out[1] |= 1L << ((in >>  4) & 63L);
  out[2] |= 1L << ((in >>  8) & 63L);
  out[3] |= 1L << ((in >> 12) & 63L);
}
INLINE int bmatch(long *filter, long in) {
  return
  filter[0] & (1L << ((in >>  0) & 63L)) &&
  filter[1] & (1L << ((in >>  4) & 63L)) &&
  filter[2] & (1L << ((in >>  8) & 63L)) &&
  filter[3] & (1L << ((in >> 12) & 63L));
}
#ifdef __sw_host__

#endif
#ifdef __sw_slave__
extern "C"{
#include <qthread_slave.h>
}
#include "esmd_types.h"
#include <stdio.h>
#include <simd.h>
#include "dma_macros_new.h"
#include "swarch.h"
#include "bitonic_sort_sw.h"
#include "dmapp.hpp"
//#include "cal.h"
#define LWPF_UNIT U(FIND_BONDS)
#define LWPF_KERNELS K(ALL) K(BFILTER) K(ILOOP) K(JLOOP) K(REV)
//#define EVT_PC0 PC0_CNT_INST
//#define EVT_PC1 PC1_CYCLE
//#define EVT_PC2 PC2_CNT_GLD
#include "lwpf3/lwpf.h"
__attribute_noinline__ void dummy() {
  asm volatile("memb\n\t" ::: "memory");
}

#endif
