def simd_vshff(Va, Vb, c):
  i0 = c & 3
  i1 = (c >> 2) & 3
  i2 = (c >> 4) & 3
  i3 = (c >> 6) & 3
  return [Vb[i0], Vb[i1], Va[i2], Va[i3]]
import re
SP_RE = re.compile(r"\s*,\s*")
def vec(s):
  return list(SP_RE.split(s))

v0 = vec("x0, y0, z0, x1")
v1 = vec("y1, z1, x2, y2")
l2 = vec("z2, x3, y3, z3")

t0 = simd_vshff(l2, v1, 0x6b)
print(t0)
v2 = simd_vshff(v1, v0, 0x46)
print(v2)
v0 = simd_vshff(t0, v0, 0xdc)
print(v0)
v1 = simd_vshff(t0, v2, 0x89)
print(v1)
v2 = simd_vshff(l2, v2, 0xcc)
print(v2)
# t0 = simd_vshff(a2, a1, 0x44)
# print(t0)
# t1 = simd_vshff(a2, a1, 0xee)
# print(t1)
# v1 = simd_vshff(a1, a0, 0xee)
# print(v1)
# v0 = simd_vshff(t0, a0, 0x84)
# print(v0)
# v2 = simd_vshff(v1, a2, 0xde)
# print(v2)
# v0 = simd_vshff(v0, v0, 0x78)
# print(v0)
# v1 = simd_vshff(v1, t0, 0x8d)
# print(v1)
# v2 = simd_vshff(t1, v2, 0xd8)
# print(v2)
