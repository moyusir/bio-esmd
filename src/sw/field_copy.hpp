template <typename T>
__always_inline void field_copy(T *a, const T *b) {
  *a = *b;
}
template <>
__always_inline void field_copy<vec<real>>(vec<real> * a, const vec<real> * b) {
  a->x = b->x;
  a->y = b->y;
  a->z = b->z;
}
template <>
__always_inline void field_copy<shake_t>(shake_t *a, const shake_t *b) {
  a->idx[0] = b->idx[0];
  a->idx[1] = b->idx[1];
  a->idx[2] = b->idx[2];
  a->rsq[0] = b->rsq[0];
  a->rsq[1] = b->rsq[1];
  a->rsq[2] = b->rsq[2];
  #ifdef WITH_LINCS
  a->r0[0] = b->r0[0];
  a->r0[1] = b->r0[1];
  a->r0[2] = b->r0[2];
  a->Sdiag[0] = b->Sdiag[0];
  a->Sdiag[1] = b->Sdiag[1];
  a->Sdiag[2] = b->Sdiag[2];
  #endif
  a->type = b->type;
}

template<>
__always_inline void field_copy<long[2]>(long (*a)[2], const long (*b)[2]) {
  a[0][0] = b[0][0];
  a[0][1] = b[0][1];
}
template<>
__always_inline void field_copy<int[3]>(int (*a)[3], const int (*b)[3]) {
  a[0][0] = b[0][0];
  a[0][1] = b[0][1];
  a[0][2] = b[0][2];
}