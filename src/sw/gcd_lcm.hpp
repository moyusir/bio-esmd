template <int a, int b>
struct gcd {
  static constexpr int value = gcd<b, a%b>::value;
};
template <int a>
struct gcd<a, 0> {
  static constexpr int value = a;
};
template <int a, int b>
struct lcm {
  static constexpr int value = a / gcd<a, b>::value * b;
};
// #define VEC_WIDTH 4
// struct vint {
//   long a, b, c, d;
// };
// vint v_ldl(long *v) {
//   return vint{v[0], v[1], v[2], v[3]};
// }
// vint v_stl(long *d, vint v) {
//   d[0] = v.a;
//   d[1] = v.b;
//   d[2] = v.c;
//   d[3] = v.d;
// }
// #include <cstdio>

// template <int MASKNI>
// __attribute__((always_inline)) inline void repeat_bitmask(long (*mask)[MASKNI], long *from, int len) {
//   constexpr int nseed = lcm<MASKNI, VEC_WIDTH>::value;
//   constexpr int nrepeat = nseed / MASKNI;
//   constexpr int nvector = nseed;
//   long seeds[nseed];
//   for (int i = 0; i < nrepeat; i ++) {
//     for (int j = 0; j < MASKNI; j ++) {
//       seeds[i * MASKNI + j] = from[j];
//     }
//   }
//   long *plain_mask = (long*)mask;
//   for (int i = 0; i < len; i += nrepeat) {
//     #pragma GCC unroll(VEC_WIDTH)
//     for (int j = 0; j < nseed; j += VEC_WIDTH) {
//       vint tmp = v_ldl(seeds + j);
//       v_stl(plain_mask + i * MASKNI + j, tmp);
//     }
//   }
// }
// int main(){
//   long a[5] = {1, 2, 3, 2, 5};
//   long b[64][5];
//   repeat_bitmask(b, a, 64);
//   for (int i = 0; i < 64; i ++) {
//     for (int j = 0; j < 5; j ++) {
//       printf("%d ", b[i][j]);
//     }
//     puts("");
//   }
// }