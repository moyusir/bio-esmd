import traceback
import struct
import sys
import re
import collections

import argparse
parser = argparse.ArgumentParser(description="post process gp based relocation to LDM based relocation")
parser.add_argument("input", help="Original ELF file")
parser.add_argument("output", help="Output ELF file")
args = parser.parse_args(sys.argv[1:])
# typedef struct
# {
#   unsigned char e_ident[EI_NIDENT];     /* Magic number and other info */
#   Elf64_Half    e_type;                 /* Object file type */
#   Elf64_Half    e_machine;              /* Architecture */
#   Elf64_Word    e_version;              /* Object file version */
#   Elf64_Addr    e_entry;                /* Entry point virtual address */
#   Elf64_Off     e_phoff;                /* Program header table file offset */
#   Elf64_Off     e_shoff;                /* Section header table file offset */
#   Elf64_Word    e_flags;                /* Processor-specific flags */
#   Elf64_Half    e_ehsize;               /* ELF header size in bytes */
#   Elf64_Half    e_phentsize;            /* Program header table entry size */
#   Elf64_Half    e_phnum;                /* Program header table entry count */
#   Elf64_Half    e_shentsize;            /* Section header table entry size */
#   Elf64_Half    e_shnum;                /* Section header table entry count */
#   Elf64_Half    e_shstrndx;             /* Section header string table index */
# } Elf64_Ehdr;

ELF64_HDR_FMT = "10sHHIQQQIHHHHHH"
ELF64_Hdr = collections.namedtuple('ELF64_Hdr', ['e_ident', 'e_type', 'e_machine', 'e_version',
                                                 'e_entry', 'e_phoff', 'e_shoff', 'e_flags',
                                                 'e_ehsize', 'e_phentsize', 'e_phnum', 'e_shentsize',
                                                  'e_shnum', 'e_shstrndx'])
# typedef struct
# {
#   Elf64_Word    sh_name;                /* Section name (string tbl index) */
#   Elf64_Word    sh_type;                /* Section type */
#   Elf64_Xword   sh_flags;               /* Section flags */
#   Elf64_Addr    sh_addr;                /* Section virtual addr at execution */
#   Elf64_Off     sh_offset;              /* Section file offset */
#   Elf64_Xword   sh_size;                /* Section size in bytes */
#   Elf64_Word    sh_link;                /* Link to another section */
#   Elf64_Word    sh_info;                /* Additional section information */
#   Elf64_Xword   sh_addralign;           /* Section alignment */
#   Elf64_Xword   sh_entsize;             /* Entry size if section holds table */
# } Elf64_Shdr;

ELF64_SHDR_FMT = "IIQQQQIIQQ"
ELF64_Shdr = collections.namedtuple('ELF64_Shdr', ['sh_name', 'sh_type', 'sh_flags', 'sh_addr',
                                                   'sh_offset', 'sh_size', 'sh_link', 'sh_info',
                                                   'sh_addralign', 'sh_entsize'])
# typedef struct
# {
#   Elf64_Word	st_name;		/* Symbol name (string tbl index) */
#   unsigned char	st_info;		/* Symbol type and binding */
#   unsigned char st_other;		/* Symbol visibility */
#   Elf64_Section	st_shndx;		/* Section index */
#   Elf64_Addr	st_value;		/* Symbol value */
#   Elf64_Xword	st_size;		/* Symbol size */
# } Elf64_Sym;


#define ELF32_ST_BIND(val)		(((unsigned char) (val)) >> 4)
#define ELF32_ST_TYPE(val)		((val) & 0xf)
#define ELF32_ST_INFO(bind, type)	(((bind) << 4) + ((type) & 0xf))

#define ELF64_ST_BIND(val)		ELF32_ST_BIND (val)
#define ELF64_ST_TYPE(val)		ELF32_ST_TYPE (val)
#define ELF64_ST_INFO(bind, type)	ELF32_ST_INFO ((bind), (type))

ELF64_ST_BIND = lambda x: (x & 0xff) >> 4
ELF64_ST_TYPE = lambda x: x & 0xf
ELF64_ST_INFO = lambda bind, type: bind << 4 | type & 0xf

STB_LOCAL      = 0               # Local symbol */
STB_GLOBAL     = 1               # Global symbol */
STB_WEAK       = 2               # Weak symbol */
STB_NUM        = 3               # Number of defined types.  */
STB_LOOS       = 10              # Start of OS-specific */
STB_GNU_UNIQUE = 10              # Unique symbol.  */
STB_HIOS       = 12              # End of OS-specific */
STB_LOPROC     = 13              # Start of processor-specific */
STB_HIPROC     = 15              # End of processor-specific */

STT_NOTYPE     = 0               # Symbol type is unspecified */
STT_OBJECT     = 1               # Symbol is a data object */
STT_FUNC       = 2               # Symbol is a code object */
STT_SECTION    = 3               # Symbol associated with a section */
STT_FILE       = 4               # Symbol's name is file name */
STT_COMMON     = 5               # Symbol is a common data object */
STT_TLS        = 6               # Symbol is thread-local data object*/
STT_NUM        = 7               # Number of defined types.  */
STT_LOOS       = 10              # Start of OS-specific */
STT_GNU_IFUNC  = 10              # Symbol is indirect code object */
STT_HIOS       = 12              # End of OS-specific */
STT_LOPROC     = 13              # Start of processor-specific */
STT_HIPROC     = 15		# End of processor-specific */


#define STN_UNDEF	0		/* End of a chain.  */

ELF64_SYM_FMT = "IBBHQQ"
ELF64_Sym = collections.namedtuple('ELF64_Sym', ['st_name', 'st_info', 'st_other', 'st_shndx', 'st_value', 'st_size'])

class ELF64:
    def __init__(self, bin):
        bin = bytearray(bin)
        ehdr = ELF64_Hdr(*struct.unpack(ELF64_HDR_FMT, bin[0:64]))
        self.bin = bin
        self.ehsize    = ehdr.e_ehsize
        self.phoff     = ehdr.e_phoff
        self.phnum     = ehdr.e_phnum
        self.phentsize = ehdr.e_phentsize
        self.shoff     = ehdr.e_shoff
        self.shnum     = ehdr.e_shnum
        self.shentsize = ehdr.e_shentsize

        shdr_end = self.shoff + self.shnum * self.shentsize
        shdr = list(map(lambda x: ELF64_Shdr(*x), struct.iter_unpack(ELF64_SHDR_FMT, bin[self.shoff : shdr_end])))
        shstrtabhdr = shdr[ehdr.e_shstrndx]
        #shstrdict = ELF64.split_strtab(
        shstrtab = ELF64.get_section_bin(bin, shstrtabhdr)
        self.shstrtab = shstrtab
        #print(shstrtab[341:351], len(shstrtab))
        #print("TTT:", shstrtab[341:20])
        self.shdrs = {}
        self.ishdrs = []
        for hdr in shdr:
            # print(hdr, hdr.sh_name, ELF64.get_str_from_tab(shstrtab, hdr.sh_name))
            self.shdrs[ELF64.get_str_from_tab(shstrtab, hdr.sh_name)] = hdr
            self.ishdrs.append(hdr)
        symtabhdr = self.shdrs['.symtab']
        symtabbin = ELF64.get_section_bin(bin, symtabhdr)
        syms = list(map(lambda x: ELF64_Sym(*x), struct.iter_unpack(ELF64_SYM_FMT, symtabbin)))
        strtabhdr = self.shdrs['.strtab']
        #strdict = ELF64.split_strtab(
        
        strtab = ELF64.get_section_bin(bin, strtabhdr)
        self.strtab = strtab
        #self.sym_name = {}
        self.syms = list(map(lambda sym: sym._replace(st_name = ELF64.get_str_from_tab(strtab, sym.st_name)), syms))
        self.global_syms = {}
        for sym in self.syms:
            if ELF64_ST_BIND(sym.st_info) in [STB_GLOBAL, STB_WEAK]:
                self.global_syms[sym.st_name] = sym
    @staticmethod
    def get_str_from_tab(tab, index):
        return tab[index:].split(b"\0", 1)[0].decode()

    @staticmethod
    def get_section_bin(elf_bin, sec_hdr):
        return elf_bin[sec_hdr.sh_offset : sec_hdr.sh_offset + sec_hdr.sh_size]
import enum
class SW5Rel(enum.Enum):
    NONE       = 0  # /* No reloc */
    REFLONG    = 1  # /* Direct 32 bit */
    REFQUAD    = 2  # /* Direct 64 bit */
    GPREL32    = 3  # /* GP relative 32 bit */
    LITERAL    = 4  # /* GP relative 16 bit w/optimization */
    LITUSE     = 5  # /* Optimization hint for LITERAL */
    GPDISP     = 6  # /* Add displacement to GP */
    BRADDR     = 7  # /* PC+4 relative 23 bit shifted */
    HINT       = 8  # /* PC+4 relative 16 bit shifted */
    SREL16     = 9  # /* PC relative 16 bit */
    SREL32     = 10 # /* PC relative 32 bit */
    SREL64     = 11 # /* PC relative 64 bit */
    GPRELHIGH  = 17 # /* GP relative 32 bit, high 16 bits */
    GPRELLOW   = 18 # /* GP relative 32 bit, low 16 bits */
    GPREL16    = 19 # /* GP relative 16 bit */
    COPY       = 24 # /* Copy symbol at runtime */
    GLOB_DAT   = 25 # /* Create GOT entry */
    JMP_SLOT   = 26 # /* Create PLT entry */
    RELATIVE   = 27 # /* Adjust by program base */
    TLS_GD_HI  = 28
    TLSGD      = 29
    TLS_LDM    = 30
    DTPMOD64   = 31
    GOTDTPREL  = 32
    DTPREL64   = 33
    DTPRELHI   = 34
    DTPRELLO   = 35
    DTPREL16   = 36
    GOTTPREL   = 37
    TPREL64    = 38
    TPRELHI    = 39
    TPRELLO    = 40
    TPREL16    = 41
    LDMLO      = 42
    LDMHI      = 43
MANGLE_RE = re.compile(r"_Z(?P<len>\d+)slave_(?P<rest>.*)")
def remangle_slave(mangled_name):
    m = MANGLE_RE.match(target_sym.st_name)
    if m:
        remangle = "slave__Z%d%s" % (int(m["len"]) - 6, m["rest"])
        return remangle
    else:
        return None
elf = ELF64(open(args.input, "rb").read())
text1_offset = elf.shdrs['.text1'].sh_offset
text1_addr = elf.shdrs['.text1'].sh_addr
text1_size = elf.shdrs['.text1'].sh_size
for i, s in enumerate(elf.ishdrs):
    if s == elf.shdrs['.text1']:
        text1_shndx = i
got_offset = elf.shdrs['.got'].sh_offset
got_addr = elf.shdrs['.got'].sh_addr

# sys.exit(0)
gpval = 0
rsymtab = {}
symtab = {}
for sym in elf.syms:
    rsymtab.setdefault(sym.st_value, []).append(sym)
    symtab[sym.st_name] = sym


nonliterals = set([])
if '.rela.text1' in elf.shdrs:
    relatext1_offset = elf.shdrs[".rela.text1"].sh_offset
    relatext1_size = elf.shdrs[".rela.text1"].sh_size
    for i in range(relatext1_offset, relatext1_offset + relatext1_size, 24):
        addr, info, off = struct.unpack("<QQQ", elf.bin[i:i+24])
        target = info >> 32
        reltype = SW5Rel(info & 63)
        if reltype != SW5Rel.LITERAL:
            nonliterals.add(addr)
if '.rela.text' in elf.shdrs:
    relatext1_offset = elf.shdrs[".rela.text"].sh_offset
    relatext1_size = elf.shdrs[".rela.text"].sh_size
    for i in range(relatext1_offset, relatext1_offset + relatext1_size, 24):
        addr, info, off = struct.unpack("<QQQ", elf.bin[i:i+24])
        target = info >> 32
        reltype = SW5Rel(info & 63)
        target_sym = elf.syms[target]
        if target_sym.st_shndx < len(elf.ishdrs) and elf.ishdrs[target_sym.st_shndx].sh_type == 0:
            if remangle_slave(elf.syms[target].st_name):
                print(reltype, elf.syms[target], symtab[remangle_slave(elf.syms[target].st_name)])
if 'text1_got' in symtab:
    got1_offset = symtab['text1_got'].st_value - elf.shdrs['.ldm'].sh_addr + elf.shdrs['.ldm'].sh_offset
    got1_addr = symtab['text1_got'].st_value - 0x5000004000
    got1_size = symtab['text1_got'].st_size
    has_got1 = True
else:
    has_got1 = False
RA = 26
T12 = 27
GP = 29
LDI = 0x3e
LDIH = 0x3f
LDL = 0x23
# def get_inst(i):
#     print(text1_offset + i)
#     return struct.unpack('I', elf.bin[text1_offset + i:text1_offset + i+4])[0]
def get_inst(i):
    return struct.unpack('I', elf.bin[i:i+4])[0]
got1_map = {}
for i in range(0, text1_size, 4):
    # inst = sw5insts.parse_inst(get_inst(i))
    inst = get_inst(text1_offset + i)
    op = inst >> 26
    ra = inst >> 21 & 31
    rb = inst >> 16 & 31
    disp = inst & 65535
    if disp > 32767:
        disp -= 65536
    if op == LDIH and ra == GP and rb in (T12, RA):
        gpval = i + text1_addr + disp * 65536
    if op == LDI and ra == GP and rb == GP:
        gpval = gpval + disp
    if op == LDL and rb == GP and i + text1_addr not in nonliterals:
        target_addr = gpval + disp
        target_offset = target_addr - got_addr + got_offset
        val = struct.unpack('Q', elf.bin[target_offset:target_offset + 8])[0]
        if val not in got1_map:
            got1_map[val] = len(got1_map)
            idx = got1_map[val]
            if has_got1:
                elf.bin[got1_offset + idx * 8: got1_offset + idx * 8 + 8] = struct.pack('Q', val)
        if has_got1:
            ldm_disp = got1_addr + got1_map[val] * 8
            inst_new = LDL << 26 | ra << 21 | 31 << 16 | ldm_disp & 65535
            elf.bin[text1_offset + i:text1_offset+i+4] = struct.pack('I', inst_new)
print("There is totally %d literals in text1_got" % len(got1_map), file=sys.stderr)
if not has_got1:
    print("text1_got not found in symbol table, you should define a variable \"__thread_local long text1_got[>=%d]\" in your code." % len(got1_map), file=sys.stderr)
    sys.exit(1)
if len(got1_map) * 8 > got1_size:
    print("Total number of literals %d exceeds text1_got size %d， try increase to \"__thread_local long text1_got[>=%d]\" in your code." % (len(got1_map), got1_size / 8, len(got1_map)), file=sys.stderr)
    sys.exit(1)
    
open(args.output, "wb").write(elf.bin)
import os
import stat
st = os.stat(args.output)
os.chmod(args.output, st.st_mode | stat.S_IEXEC)

# print(symtab["slave___errno_location"])
#print(gotrels)
#print(reltypes)