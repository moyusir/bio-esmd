#ifndef BITONIC_SORT_SW5_H_
#define BITONIC_SORT_SW5_H_
//function signatures
#ifdef __sw_host__
void bitonic_sort(long *tag, int n);
#endif
#ifdef __sw_slave__
void bitonic_sort(long *arr, int n);
#endif
//end function signatures
#endif
