import numpy
load = numpy.loadtxt("load.csv", delimiter=",")
cycles = load[:63].copy()
pairs = load[63:].copy()
y = cycles[:, 0] / pairs[:, 1]
x = pairs[:, 0] / pairs[:, 1]
b1 = (numpy.sum(x * y) - 63 * (x.mean() * y.mean())) / (numpy.sum(x * x) - 63 * (x.mean() * x.mean()))
b0 = y.mean() - b1 * x.mean()
print(b0, b1, b0 / b1)