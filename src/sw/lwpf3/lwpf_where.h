#ifndef LWPF_WHERE
#define LWPF_WHERE

/* We want to know where we are */
#if defined(__sw_64_thl__) || defined(__sw_64_sw2__)
#ifndef SW5
#define SW5
#endif
#else
/* determine SW7/9 by LDM size */
#if defined(__sw7__)
#define SW7
#elif defined(__sw9__)
#define SW9
#else /* MAX_LDM_SIZE */
#define SW5
// #error "Cannot determine architecture"
#endif
#endif /*__sw_64_thl__*/

#ifdef SW5 /* define the macros for CPEs */
#if !defined(__sw_host__) && !defined(__sw_slave__)
#if defined(SW2)
#define __sw_host__
#else
#define __sw_slave__
#endif /* SW2 */
#endif
#endif /* SW5 */

#endif /*LWPF_WHERE*/
