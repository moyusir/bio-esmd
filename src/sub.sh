# bsub -I -q q_sw_share -share_size 4096 -b -cgsp 64 -n 1 -p -sw3run swrun-5a -sw3runarg "-f" ../bin/esmd ../ubq_wb/ubq_wb_eq.conf
# mv backtrace.000000.bin ubq_wb_noshake.bin
# mv perf.000000.log ubq_wb_noshake.log
# bsub -I -q q_sw_share -share_size 4096 -b -cgsp 64 -n 1 -p -sw3run swrun-5a -sw3runarg "-f" ../bin/esmd ../ubq_wb/ubq_wb_eq_shake.conf
# mv backtrace.000000.bin ubq_wb_shake.bin
# mv perf.000000.log ubq_wb_shake.log
# bsub -I -q q_sw_share -share_size 4096 -b -cgsp 64 -n 1 -p -sw3run swrun-5a -sw3runarg "-f" ../bin/esmd ../6w9c/6w9c.conf
# mv backtrace.000000.bin 6w9c_noshake.bin
# mv perf.000000.log 6w9c_noshake.log
# bsub -I -q q_sw_share -share_size 4096 -b -cgsp 64 -n 1 -p -sw3run swrun-5a -sw3runarg "-f" ../bin/esmd ../6w9c/6w9c_shake.conf
# mv backtrace.000000.bin 6w9c_shake.bin
# mv perf.000000.log 6w9c_shake.log
bsub -I -q q_sw_share -share_size 4096 -b -cgsp 64 -n 4 -p -sw3run swrun-5a -sw3runarg "-f" ../bin/esmd ../6w9c/6w9c.conf
mv backtrace.000000.bin 6w9c_noshake-n4.bin
mv perf.000000.log 6w9c_noshake-n4.log
bsub -I -q q_sw_share -share_size 4096 -b -cgsp 64 -n 4 -p -sw3run swrun-5a -sw3runarg "-f" ../bin/esmd ../6w9c/6w9c_shake.conf
mv backtrace.000000.bin 6w9c_shake-n4.bin
mv perf.000000.log 6w9c_shake-n4.log
