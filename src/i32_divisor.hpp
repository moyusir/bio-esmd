#pragma once
#include <sys/cdefs.h>
struct i32_divisor {
  const long mul, shift, div;
  
  constexpr i32_divisor(int divisor) : mul(M(divisor)), shift(K(divisor)+32), div(divisor) {
  }
  private:
    static constexpr int K(int divisor) {
      int k = 0;
      while ((1L << (k + 1)) < divisor) k ++;
      return k;
    }
    static constexpr long M(int divisor) {
      long m = ((1L << K(divisor) + 32) + divisor - 1) / divisor;
      return m;
    }
};
template<class T>
__always_inline T operator/(T numer, i32_divisor denom) {
  return ((unsigned long)numer * denom.mul) >> denom.shift;
}
template<class T>
__always_inline T operator%(T numer, i32_divisor denom) {
  return numer - numer / denom * denom.div;
}
