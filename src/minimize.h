#ifndef MINIMIZE_H_
#define MINIMIZE_H_
#include "force_field.h"
#include "comm.h"
enum linemin_result {
  CG_SUCCESS,
  CG_DOWNHILL,
  CG_ETOL,
  CG_FTOL,
  CG_FORCEZERO,
  CG_ZEROQUAD,
  CG_ZEROALPHA,
  CG_MAXEVAL
};
#define ALPHAMAX 1
#define DMAX 0.1
#define EPS_QUAD 1e-28
#define QTOL 0.1
#define EMACH 1e-8
#define BACKSLOPE 0.4
#define ALPHA_REDUCE 0.5
typedef struct minimizer{
  real e;
  real alpha_last;
  ff_t *ff;
  mpp_t *mpp;
  real etol, ftol;
  int maxeval, neval, rigid;
} minimizer_t;

//function signatures
int post_alpha_shift(ff_t *ff, mpp_t *mpp);
void energy_force(minimizer_t *mz);
void alpha_step(minimizer_t *mz, real alpha_new);
int linemin_quadratic(minimizer_t *mz, real eorig);
int min_cg(minimizer_t *mz, int niter);
void init_minimizer(minimizer_t *mz, int maxeval, real etol, real ftol, ff_t *ff, mpp_t *mpp);
//end function signatures
#endif
