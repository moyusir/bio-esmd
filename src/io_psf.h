#ifndef IO_PSF_H_
#define IO_PSF_H_
#include "esmd_types.h"
#include <stdio.h>

// typedef struct psf_bond {
//   long i, j;
// } psf_bond_t;
// typedef struct psf_angle {
//   long i, j, k;
// } psf_angle_t;
// typedef struct psf_dihed {
//   long i, j, k, l;
// } psf_dihed_t;

typedef struct psf_atom {
  elemstr type;
  double charge, mass;
  long id;
} psf_atom_t;

typedef struct psf_data {
  psf_atom_t *atom;
  bond_t *bond, *don, *acc, *excl;
  angle_t *angle;
  dihed_t *tori, *impr;
  long natom, nbond, nangle, ntori, nimpr, ndon, nacc, nexcl;
} psf_data_t;

//function signatures
psf_data_t *read_psf(FILE *fpsf) ;
void free_psf(psf_data_t *psf) ;
//end function signatures
#endif
