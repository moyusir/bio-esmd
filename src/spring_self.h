#pragma once
#include "enhance.h"
#include "cell.h"
struct spring_self : enhance_t {
  int mol;
  real K;
  void pre_force(cellgrid_t *grid, mpp_t *) override {
  }
  void post_force(cellgrid_t *grid, mpp_t *) override {
    FOREACH_LOCAL_CELL(grid, cx, cy, cz, cell) {
      for (int i = 0; i < cell->natom; i ++){
        if (cell->mol[i] == mol){
          vec<real> d = cell->x[i] + cell->basis - cell->xinit[i];
          real r = d.norm();
          if (r > 0){
            vec<real> f = -2.0 * K * d / r;
            cell->f[i] += f;
          }
        }
      }
    }
  };
};
