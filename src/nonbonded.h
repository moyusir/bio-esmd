#ifndef NONBONDED_H_
#define NONBONDED_H_
#include "esmd_types.h"
typedef struct vdw_param {
  real eps, sig, eps14, sig14;
} vdw_param_t;
typedef struct nonbonded_param {
  int ntypes;
  real rsw, rcut;
  real scale14, coul_const;
  int coultype;
  real nonb_threshold;
  vdw_param_t *vdw_param;
  int order;
} nonbonded_param_t; // function signatures
struct cellgrid_t;

void nonbonded_force_lj_msm(cellgrid_t *grid, nonbonded_param_t *p, mdstat_t *stat);
void nonbonded_force_lj_shifted(cellgrid_t *grid, nonbonded_param_t *p, mdstat_t *stat);
//function signatures
void nonbonded_force_lj_shifted(cellgrid_t *grid, nonbonded_param_t *p, mdstat_t *stat);
void nonbonded_force_lj_msm(cellgrid_t *grid, nonbonded_param_t *p, mdstat_t *stat);
void nonbonded_force_lj_rf(cellgrid_t *grid, nonbonded_param_t *p, mdstat_t *stat);
//end function signatures
#endif
