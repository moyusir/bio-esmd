#ifndef IO_CHARMM_INP_H_
#define IO_CHARMM_INP_H_
#include <stdio.h>
#include "esmd_types.h"
#include "list.h"
typedef struct charmm_raw_bond_param {
  elemstr itype, jtype;
  double kr, r0;
} charmm_raw_bond_param_t;

typedef struct charmm_raw_angle_param {
  elemstr itype, jtype, ktype;
  double ktheta, theta0, kub, s0;
} charmm_raw_angle_param_t;

typedef struct charmm_raw_dihed_param {
  elemstr itype, jtype, ktype, ltype;
  double kphi, n, phi0;
} charmm_raw_dihed_param_t;

typedef struct charmm_raw_nonb_param {
  elemstr itype;
  double emin, rmin, emin14, rmin14;
} charmm_raw_nonb_param_t;

typedef struct charmm_rawparam {
  charmm_raw_bond_param_t *bond;
  charmm_raw_angle_param_t *angle;
  charmm_raw_dihed_param_t *tori, *impr;
  charmm_raw_nonb_param_t *nonb;
  int nbond, nangle, ntori, nimpr, nnonb;
} charmm_rawparam_t;

//function signatures
charmm_rawparam_t *read_inp(FILE *finp) ;
//end function signatures
#endif
