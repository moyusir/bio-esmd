#ifndef MAIN_TEST_H_
#define MAIN_TEST_H_
//function signatures
size_t file_size(FILE *);
void bonded_debug(long, long, bond_param_t *);
void angle_debug(long, long, long, angle_param_t *);
void torsion_debug(long, long, long, long, tori_param_t *, int);
void improper_debug(long, long, long, long, harmonic_impr_param_t *);
real bonded_force(struct vec_real *, struct vec_real *, struct vec_real *, struct vec_real *, bond_param_t *);
real angle_force(struct vec_real *, struct vec_real *, struct vec_real *, struct vec_real *, struct vec_real *, struct vec_real *, angle_param_t *);
real torsion_force(struct vec_real *, struct vec_real *, struct vec_real *, struct vec_real *, struct vec_real *, struct vec_real *, struct vec_real *, struct vec_real *, tori_param_t *, int);
real improper_force(struct vec_real *, struct vec_real *, struct vec_real *, struct vec_real *, struct vec_real *, struct vec_real *, struct vec_real *, struct vec_real *, harmonic_impr_param_t *);
void bonded_force_stub(cellgrid_t *, int, bond_param_t *, angle_param_t *, tori_param_tab_t *, harmonic_impr_param_t *);
void nonbonded_force(cellgrid_t *, real, real, real, real, real);
int main();
//end function signatures
#endif
