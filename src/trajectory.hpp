#include "cell.h"
#include "comm.h"
#include "elemtab.h"
void write_elemtab(const char *path, elemtab_t *elemtab);
void read_elemtab(const char *path, elemtab_t *elemtab);
void write_grid(const char *path, cellgrid_t *grid);
void read_grid(const char *path, cellgrid_t *grid);
long read_restart(const char *prefix, mpp_t *mpp, cellgrid_t *grid, elemtab_t *elemtab);
void write_restart(const char *prefix, int itape, long istep, mpp_t *mpp, cellgrid_t *grid);
void write_trajectory(const char *path, long istep, cellgrid_t *grid);