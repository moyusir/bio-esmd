#ifndef VERLET_H_
#define VERLET_H_
#include "esmd_conf.h"
#include "force_field.h"
#include "rigid.h"
//function signatures
int initial_integrate_verlet(cellgrid_t *grid, real dt, real vscale);
void final_integrate_verlet(cellgrid_t *grid, real dt);
int post_coordinate_update(ff_t *ff, mpp_t *mpp);
void integrate_verlet(long step_start, ff_t *ff, rigid_t *rigid, mpp_t *mpp, esmd_config_t *conf);
void check_nan(ff_t *ff);
//end function signatures
#endif
