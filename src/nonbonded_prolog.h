  /* this header file yields r2, scaled and excluded for computing nonbonded interactions with various methods*/
  /* if excluded particles also needs processing (e.g. subtracting long ranged coulomb), define NO_SKIP_EXCL before include*/
  real rcut = p->rcut;
  real inv_rcut = 1. / rcut;
  real rsw = p->rsw;
  real scale14 = p->scale14;
  real coul_const = p->coul_const;
  vdw_param_t *vdw_param = p->vdw_param;
  real nonb_threshold = p->nonb_threshold;
  real rcut2 = rcut * rcut;
  real rsw2 = rsw * rsw;
  real inv_rcut2 = 1. / rcut2;
  real denom_switch = rcut2 - rsw2;
  real inv_denom_switch = 1 / (denom_switch * denom_switch * denom_switch);
  int order = p->order;
  double totalw = 0, totalu = 0;

  FOREACH_LOCAL_CELL(grid, ii, jj, kk, icell) {
    
    FOREACH_NEIGHBOR_HALF_SHELL(grid, ii, jj, kk, dx, dy, dz, jcell) {
      
      int jcell_id = jcell - grid->cells;
      int did = hdcell(grid->nn, dx, dy, dz);
      vec<real> dcell;
      vecsubv(dcell, icell->basis, jcell->basis);
      // real epsi[CELL_CAP], sigi[CELL_CAP], epsi14[CELL_CAP], sigi14[CELL_CAP];
      real epsj[CELL_CAP], sigj[CELL_CAP], epsj14[CELL_CAP], sigj14[CELL_CAP];
      for (int j = 0; j < jcell->natom; j++) {
        epsj[j] = vdw_param[jcell->t[j]].eps;
        sigj[j] = vdw_param[jcell->t[j]].sig;
        epsj14[j] = vdw_param[jcell->t[j]].eps14;
        sigj14[j] = vdw_param[jcell->t[j]].sig14;
      }

      /*this is a marker that indicates the most recent i atom has excl/scal with index j*/
      int is_excl[CELL_CAP];
      int is_scal[CELL_CAP];
      
      for (int j = 0; j < jcell->natom; j ++) {
        is_excl[j] = -1;
        is_scal[j] = -1;
      }

      int ie = icell->first_excl_cell[did];
      int is = icell->first_scal_cell[did];
      for (int i = 0; i < icell->natom; i++) {
        while (ie < icell->first_excl_cell[did + 1] && icell->excl_id[ie][0] <= i) {
          is_excl[icell->excl_id[ie][1]] = icell->excl_id[ie][0];
          ie ++;
        }
        while (is < icell->first_scal_cell[did + 1] && icell->scal_id[is][0] <= i) {
          is_scal[icell->scal_id[is][1]] = icell->scal_id[is][0];
          is ++;
        }
        real sigi = vdw_param[icell->t[i]].sig;
        real epsi = vdw_param[icell->t[i]].eps;
        real sigi14 = vdw_param[icell->t[i]].sig14;
        real epsi14 = vdw_param[icell->t[i]].eps14;
        int nj = jcell->natom;
        if (dx == 0 && dy == 0 && dz == 0)
          nj = i;
        for (int j = 0; j < nj; j++) {
#ifndef NO_SKIP_EXCL
          if (is_excl[j] == i)
            continue;
#else
          int excluded = (is_excl[j] == i);
#endif
          int scaled = (is_scal[j] == i);
          vec<real> datom;
          vecsubv(datom, icell->x[i], jcell->x[j]);
          vecaddv(datom, datom, dcell);
          real r2 = vecnorm2(datom);
          if (r2 < rcut2) {