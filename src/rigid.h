#ifndef RIGID_H_
#define RIGID_H_
#include "cell.h"
#include "comm.h"
typedef void rigid_apply(cellgrid_t *grid, mpp_t *mpp, real dt, real ftm2v);
typedef struct rigid {
  int mask;
  rigid_apply *setup, *post_force, *final_integrate;
} rigid_t;
enum rigid_masks{
  RIGID_POST_FORCE = 1,
  RIGID_FINAL_INTEGRATE = 2
};
//function signatures
//end function signatures
#endif
