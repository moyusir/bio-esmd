#include "shake.hpp"
#include "rigid.hpp"
#include "rigid_impl.hpp"
shake shake_obj;
void shake_setup(cellgrid_t *grid, mpp_t *mpp, real dt, real ftm2v) {
  rigid_setup(grid, mpp, dt, ftm2v, shake_obj);
}
void shake_post_force(cellgrid_t *grid, mpp_t *mpp, real dt, real ftm2v) {
  rigid_post_force(grid, mpp, dt, ftm2v, shake_obj);
}
