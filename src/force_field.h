#ifndef FORCE_FIELD_H_
#define FORCE_FIELD_H_
#include "cell.h"
#include "comm.h"
#include "enhance.h"
#include "list_cpp.hpp"
typedef void bonded_force_t(cellgrid_t *grid, void *param, mdstat_t *stat);
typedef void nonbonded_force_t(cellgrid_t *grid, void *param, mdstat_t *stat);
typedef void longrange_force_t(cellgrid_t *grid, void *longgrid, void *param, mdstat_t *stat);
typedef real temp_integrate_t(cellgrid_t *grid, mpp_t *mpp, void *tstat, real progress, real ke_current);
typedef enum force_mask {
  M_NONB = 1,
  M_BOND = 2,
  M_LONG = 4,
  M_POST_FORCE = 8,
  M_FINAL_INTEGRATE = 16,
  M_TEMP_INTEGRATE = 32
} force_mask;
typedef struct forcefield {
  cellgrid_t *grid;
  void *param, *longgrid, *tstat;
  bonded_force_t *bonded;
  nonbonded_force_t *nonbonded;
  longrange_force_t *longrange;
  temp_integrate_t *temp_integrate;
  mdstat_t stat;
  int use_fdotr, present;
  //enhance_t *enhs = nullptr;
  esmd::list<enhance_t *> enhs;
  mpp_t *mpp;
  forcefield():enhs("enhancements"){};
} ff_t;
//function signatures
void force_fdotr_compute(cellgrid_t *grid, mdstat_t *stat);
void force(ff_t *ff);
//end function signatures
#endif
