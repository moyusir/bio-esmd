#include "lincs.hpp"
#include "rigid.hpp"
#include "rigid_impl.hpp"
lincs lincs_obj = { 4 };
void lincs_setup(cellgrid_t *grid, mpp_t *mpp, real dt, real ftm2v) {
  rigid_setup(grid, mpp, dt, ftm2v, lincs_obj);
}
void lincs_post_force(cellgrid_t *grid, mpp_t *mpp, real dt, real ftm2v) {
  rigid_post_force(grid, mpp, dt, ftm2v, lincs_obj);
}
