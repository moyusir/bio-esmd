#ifndef BONDED_H_
#define BONDED_H_
#include "esmd_types.h"
#include "elemtab.h"
#include "io_psf.h"
#include "charmm_param.h"

struct bond_graph_t {
  int *first_bond;
  long *bonded_tag;
};

struct impr_index_t {
  int *first_impr;
  int (*impr_bid)[3];
};


/*
        There are five choices for wildcard usage for improper dihedrals;
1) A - B - C - D  (all four atoms, double specification allowed)
2) A - X - X - B
3) X - A - B - C
4) X - A - B - X
5) X - X - A - B
*/


#define himpr_type(ntypes, i, j, k, l) (((i * ntypes + j) * ntypes + k) * ntypes + l)
#define htori_type(ntypes, i, j, k, l) (((j * ntypes + k) * ntypes + i) * ntypes + l)

//function signatures
void find_bonds(cellgrid_t *grid);
void bfs_excls(long natoms, long (*excls)[MAX_EXCLS_ATOM], long (*scales)[MAX_SCALS_ATOM], bond_graph_t *graph);
void bfs_excls_chain2(long natoms, long (*excls)[MAX_EXCLS_ATOM], long (*chain2)[MAX_EXCLS_ATOM][2], long (*scales)[MAX_SCALS_ATOM], bond_graph_t *graph);
void build_graph(bond_graph_t *graph, psf_data_t *psf);
void index_impr(impr_index_t *index, bond_graph_t *graph, psf_data_t *psf);
void process_pbc(long natoms, long nprocs, bond_graph_t *graph, long (*excls)[MAX_EXCLS_ATOM], long (*chain2)[MAX_EXCLS_ATOM][2], long (*scales)[MAX_SCALS_ATOM], double (*x)[3], vec<real> *glen);
//end function signatures
#endif
