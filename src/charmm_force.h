#ifndef CHARMM_FORCE_H_
#define CHARMM_FORCE_H_
//function signatures
void bonded_debug(long itag, long jtag, bond_param_t *p);
void angle_debug(long itag, long jtag, long ktag, angle_param_t *p);
void torsion_debug(long itag, long jtag, long ktag, long ltag, tori_param_t *p, int nmult);
void improper_debug(long itag, long jtag, long ktag, long ltag, harmonic_impr_param_t *p);
real bonded_force(vec<real> * fi, vec<real> * fj, vec<real> * xi, vec<real> * xj, bond_param_t *p);
real angle_force(vec<real> * fi, vec<real> * fj, vec<real> * fk, vec<real> * xi, vec<real> * xj, vec<real> * xk, angle_param_t *p);
real torsion_force(vec<real> * fi, vec<real> * fj, vec<real> * fk, vec<real> * fl, vec<real> * xi, vec<real> * xj, vec<real> * xk, vec<real> * xl, tori_param_t *p, int nmult);
real improper_force(vec<real> * fi, vec<real> * fj, vec<real> * fk, vec<real> * fl, vec<real> * xi, vec<real> * xj, vec<real> * xk, vec<real> * xl, harmonic_impr_param_t *impr_param);
void charmm_bonded(cellgrid_t *grid, charmm_param_t *param, mdstat_t *stat);
void charmm_nonbonded(cellgrid_t *grid, charmm_param_t *param, mdstat_t *stat);
//end function signatures
#endif
