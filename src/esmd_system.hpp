#include "cell.h"
#include "force_field.h"
#include "comm.h"
struct esmd_system{
  cellgrid_t *grid;
  force_field_t *ff;
  mpp_t *mpp;
};