#ifndef POOL_H_
#define POOL_H_
//#include <data.h>
#define esmd_pool(type)                            \
  struct {                                         \
    type **pool;                                   \
    unsigned long free_cnt, total_cnt, buffer_cnt; \
    size_t elem_size;                              \
  }
#define ESMD_POOL_GROW_RATE 1.5
#define EPOOL_ES(pool_ptr) (sizeof(**(pool_ptr)->pool))
#define EPOOL_PT(pool_ptr) (typeof(*(pool_ptr)->pool))

typedef esmd_pool(void) esmd_pool_void;
#define esmd_pool_init(pool_ptr, max_cnt, name) esmd_pool_init_impl((esmd_pool_void *)(pool_ptr), max_cnt, EPOOL_ES(pool_ptr), name)
#define esmd_pool_get(pool_ptr) (EPOOL_PT(pool_ptr) esmd_pool_get_impl((esmd_pool_void *)(pool_ptr)))
#define esmd_pool_return(pool_ptr, ptr) esmd_pool_return_impl((esmd_pool_void *)(pool_ptr), (void *)(ptr))
#define esmd_pool_destroy(pool_ptr) esmd_pool_destroy_impl((esmd_pool_void *)(pool_ptr))
//function signatures
void esmd_pool_init_impl(esmd_pool_void *pool, unsigned long total_cnt, size_t elem_size, const char *name) ;
void *esmd_pool_get_impl(esmd_pool_void *pool) ;
void esmd_pool_return_impl(esmd_pool_void *pool, void *ptr) ;
void esmd_pool_destroy_impl(esmd_pool_void *pool) ;
//end function signatures
#endif
