#ifndef IO_PDB_H_
#define IO_PDB_H_
#include "esmd_types.h"
typedef struct pdb_atom {
  double x, y, z;
  long id;
} pdb_atom_t;

typedef struct pdb_data {
  pdb_atom_t *atom;
  long natom;
} pdb_data_t;

//function signatures
pdb_data_t *read_pdb(FILE *fpdb) ;
void free_pdb(pdb_data_t *pdb) ;
//end function signatures
#endif
