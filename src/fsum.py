import numpy
f = numpy.zeros((7051, 3))
fe = numpy.zeros((7051, 3))
fv = numpy.zeros((7051, 3))
for line in open("output.txt"):
  sp = line.split()
  if sp[0] == 'tori:' or sp[0] == 'impr:':
    i, j, k, l = map(int, sp[1:5])
    f00, f01, f02 = map(float, sp[5:8])
    f10, f11, f12 = map(float, sp[8:11])
    f20, f21, f22 = map(float, sp[11:14])
    f[i] += [f00, f01, f02]
    f[j] += [f10, f11, f12]
    f[j] -= [f00, f01, f02]
    f[k] += [f20, f21, f22]
    f[k] -= [f10, f11, f12]
    f[l] -= [f20, f21, f22]
  elif sp[0] == 'angle:':
    i, j, k = map(int, sp[1:4])
    f00, f01, f02 = map(float, sp[4:7])
    f10, f11, f12 = map(float, sp[7:10])
    f20, f21, f22 = map(float, sp[10:13])
    f[i] += [f00, f01, f02]
    f[j] += [f10, f11, f12]
    f[k] += [f20, f21, f22]
  elif sp[0] == 'bond:':
    i, j = map(int, sp[1:3])
    f0, f1, f2 = map(float, sp[3:6])
    f[i] += [f0, f1, f2]
    f[j] -= [f0, f1, f2]
  elif sp[0] == 'nonb:':
    i, j = map(int, sp[1:3])
    f0, f1, f2 = map(float, sp[3:6])
    fv[i] += [f0, f1, f2]
    fv[j] -= [f0, f1, f2]
    f0, f1, f2 = map(float, sp[6:9])
    fe[i] += [f0, f1, f2]
    fe[j] -= [f0, f1, f2]
  elif sp[0] == 'nonb-excl:':
    i, j = map(int, sp[1:3])
    f0, f1, f2 = map(float, sp[3:6])
    fv[i] -= [f0, f1, f2]
    fv[j] += [f0, f1, f2]
    f0, f1, f2 = map(float, sp[6:9])
    fe[i] -= [f0, f1, f2]
    fe[j] += [f0, f1, f2]
  elif sp[0] == 'nonb-scal:':
    i, j = map(int, sp[1:3])
    f0, f1, f2 = map(float, sp[3:6])
    fv[i] += [f0, f1, f2]
    fv[j] -= [f0, f1, f2]
