#ifndef IO_DCD_H_
#define IO_DCD_H_
#include "comm.h"
typedef struct dcd_header {
  int32_t nbhead_begin;  /* = 84 (bytes for "head" section) */
  char title[4];         /* = "CORD" */
  int32_t numframes;     /*  number of frames, updated as we write */
  int32_t firststep;     /*  first step number */
  int32_t framestepcnt;  /*  number of steps per frame */
  int32_t numsteps;      /*  total number sim steps + first step, updated */
  int32_t zero5[5];      /* = 0 */
  float timestep;        /*  time step */
  int32_t iscell;        /* = 1 if unitcell info, = 0 if not */
  int32_t zero8[8];      /* = 0 */
  int32_t charmmversion; /* = 24 (CHARMM 24 format DCD file) */
  int32_t nbhead_end;    /* = 84 (bytes for "head" section) */

  int32_t nbtitle_begin; /* = 164 (bytes for "title" section) */
  int32_t numtitle;      /* = 2 (i.e. two 80-char title strings) */
  char title_str[80];    /*  remarks title string */
  char create_str[80];   /*  remarks create time string */
  int32_t nbtitle_end;   /* = 164 (bytes for "title" section) */

  int32_t nbnatoms_begin; /* = 4 (bytes for "natoms" section) */
  int32_t natoms;         /*  number of atoms */
  int32_t nbnatoms_end;   /* = 4 (bytes for "natoms" section) */
} dcd_header_t;
typedef struct dcd_cell {
  int32_t dummy1;       /*  (padding to 8-byte word alignment) */
  int32_t nbcell_begin; /* = 48 (bytes for "unitcell" section) */
  double unitcell[6]; /*  describes periodic cell */
  int32_t nbcell_end;   /* = 48 (bytes for "unitcell" section) */
  int32_t dummy2;       /*  (padding to 8-byte word alignment) */
} dcd_cell_t;
typedef struct dcd_file {
  dcd_header_t header;
  dcd_cell_t cell;
  FILE *file;
  int32_t *coordbuf;
} dcd_file_t;
//function signatures
void dcd_header_init(dcd_file_t *dcd, const char *path, cellgrid_t *grid, mpp_t *mpp, real dt);
void dcd_write_frame(dcd_file_t *dcd, cellgrid_t *grid, mpp_t *mpp);
void dcd_close(dcd_file_t *dcd, mpp_t *mpp);
//end function signatures
#endif
