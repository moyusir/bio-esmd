#ifndef NOSE_HOOVER_H_
#define NOSE_HOOVER_H_
#include "esmd_conf.h"
#include "esmd_types.h"
#include "comm.h"
#define MAX_TCHAIN 8
typedef struct nh_tstat {
  real tstart, tstop, tfreq, tdamp, drag, tdrag_factor, ncfac, tfactor;
  real ftm2v, mvv2e, boltz, dt, dtf;
  int mtchain, nc_tchain, tdof;
  real eta[MAX_TCHAIN], eta_mass[MAX_TCHAIN], eta_dot[MAX_TCHAIN + 1], eta_dotdot[MAX_TCHAIN];
} nh_tstat_t;
//function signatures
void nh_init(nh_tstat_t *tstat, cellgrid_t *grid, esmd_config_t *conf);
real nh_get_ke(cellgrid_t *grid, mpp_t *mpp, nh_tstat_t *tstat);
real nh_temp_integrate(cellgrid_t *grid, mpp_t *mpp, nh_tstat_t *tstat, real progress, real ke_current);
//end function signatures
#endif
